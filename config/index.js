module.exports = {
    medicOrgsSpreadSheetId: '1svTFVSU44vNjkk-QUa6QTzJueQI6kj63Yu8JMKeQOD8',
    reportsSize: 50,
    viewSize: 10,

    workDirMgt: 'work/mgt',
    workDirReports: 'work/reports',
    workDirMedicsByArea: 'work/medicsByArea',

    testUserId: '1647',
    testUserConfig: '456789'
}
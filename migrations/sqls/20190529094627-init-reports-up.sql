CREATE TABLE IF NOT EXISTS work_status(
    id serial PRIMARY KEY,
    status character varying(20) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE work_status
  OWNER TO reports;

CREATE TABLE IF NOT EXISTS users(
    id serial PRIMARY KEY,
    role_id smallint NOT NULL,
    salt character varying(40) NOT NULL,
    login character varying(255) NOT NULL,
    hashed_pass text NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO reports;

CREATE TABLE IF NOT EXISTS reports_status(
    id serial PRIMARY KEY,
    status smallint NOT NULL DEFAULT 1,
    query text NOT NULL,
    name character varying(255) NOT NULL,
    client_id smallint REFERENCES users(id) ON DELETE CASCADE,
    creation_date timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE reports_status
  OWNER TO reports;



CREATE TABLE IF NOT EXISTS users_roles
(
    id serial NOT NULL,
    role_name character varying(20)
)
WITH (
    OIDS = FALSE
);
ALTER TABLE users_roles
    OWNER to reports;

INSERT INTO users_roles(role_name) VALUES ('user'), ('admin');
INSERT INTO work_status(status) VALUES ('free'), ('busy'), ('success'), ('fail');
INSERT INTO users(salt, hashed_pass, login, role_id) VALUES ('8c7d0f5354fe8c3aba56f3e393be8f21048d90ee', '50b451ff3ad810ed470b2e8c939edfda374147b17f1569c4266daa6ac2e34aa9', 'g.potapov@medpoint24.ru', 2);
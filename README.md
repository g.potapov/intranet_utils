# Prepare

    npm i
    npm i -g pm2             ## Install pm2 globally (root) **for dev**
    npm i -g db-migrate   ## Install db-migrate-pg globlly (root)
    npm i -g db-migrate-pg   ## Install db-migrate-pg globlly (root)
    npm i -g dotenv          ## Install dotenv globally (root)

    check user ~/.bash_profile, if no, set:

    DB_HOST=10.255.2.101
    DB_PORT=5432

## Format .env file:

    PORT = %your app port%

    ONLY_READ_MEDIST_DB = postgresql://user:password@host:port/dbname
    MEDIST_DB = postgresql://user:password@host:port/dbname
    REPORTS_DB = postgresql://user:password@host:port/dbname

    TOKEN_PRIVATE_KEY = %generate your token key%

    MAILING_SERVICE_URL = %url to mailing service%
    FILEGEN_MEDIATOR_URL = %url to filegen mediator service%

    save in project root directory    


# Prepared DB

    CREATE DATABASE reports;
    CREATE USER reports WITH ENCRYPTED PASSWORD 'qwerty';
    GRANT ALL PRIVILEGES ON DATABASE reports TO reports;

    Then run ./migrate.sh check 
             ./migrate.sh up 

 
# Prepare for nginx
    Укажите домен в файле ~/app/intranet_utilites/public/config.js
    
    Скопируйте конфиг микросервиса ~/app/intranet_utilites/intranet.conf /etc/nginx/conf.d/
    
  **Don't forget check config before restart** 
  
    # nginx -t 
    # systemctl reload nginx
    

# DEV start 

## dev Start through pm2

    pm2 start dev.config.js


## Show current pm2 status
    
    pm2 ls


## Restart service throw pm2

    pm2 restart %id%


## Stop service throw pm2

    pm2 stop %id%


# Prod start

   **User must consist in group wheel or have extended rules in /etc/sudoers**
   
    cd path/to/your/project
    
    sudo cp intranet-utils.service intranet-worker.service /etc/systemd/system
    sudo systemctl daemon-reload
    sudo systemctl enable intranet-utils.service && systemctl enable intranet-worker.service
    sudo systemctl start intranet-utils.service && systemctl start intranet-worker.service


# Prod reload

   **User must consist in group wheel or have extended rules in /etc/sudoers**
   
    sudo systemctl restart intranet-utils.service && systemctl restart intranet-worker.service
    
# Cron systemd service and timer

   **Create systemd service in /etc/systemd/system/intranet-cron.service:**

    [Unit]
    Description=intranet cron service
    Requires=network.target
    Documentation=https://git@gitlab.medpoint24.ru:6324/arcius/intranet_utilites.git
    Wants=network-online.target
    After=network.target network-online.target
    
    [Service]
    User=wilton
    Group=wilton
    Type=simple
    ExecStart=/usr/bin/node  app/bin/emailCron.js
    WorkingDirectory=/your/project/path/intranet_utilites
    Restart=on-failure
    LimitNOFILE=infinity
    LimitCORE=infinity
    StandardInput=null
    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=intranet-cron
    PIDFile=/var/run/intranet-cron.pid
    
    [Install]
    WantedBy=multi-user.target network-online.target
    
   **Create systemd timer in /etc/systemd/system/intranet-cron.timer:**

    [Unit]
    Description=Intranet timer

    [Timer]
    OnCalendar=*-*-* 00:08:00
    Unit=intranet-cron.service
    Persistent=true
    
    [Install]
    WantedBy=multi-user.target

    systemctl daemon-reload
    systemctl enable intranet-cron.timer && systemctl start intranet-cron.timer
   **Check timers list**
    
    systemctl list-timers    


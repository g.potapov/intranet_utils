const timezoneEnv = {
    TZ: 'Europe/Moscow',
    PORT: 3086
};

const logPath = filename => `logs/${filename}.log`;

const LOG_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSS Z';


module.exports = {
    apps: [
        {
            watch: ['app'],
            name: 'intranet_utilites',
            script: 'app/bin/www',
            log_file: logPath('iu-log'),
            out_file: logPath('iu-out'),
            error_file: logPath('iu-error'),
            log_date_format: LOG_DATE_FORMAT,
            env: timezoneEnv,
        },

        {
            watch: ['bin/reportWorker.js'],
            name: 'report_worker',
            script: 'app/bin/reportWorker.js',
            log_file: logPath('reportWorker-log'),
            out_file: logPath('reportWorker-out'),
            error_file: logPath('reportWorker-error'),
            log_date_format: LOG_DATE_FORMAT,
        }

    ]
};
import Vue from 'vue';
import './plugins/vuetify';
import VueInputMask from 'vue-inputmask';
import Vuelidate from 'vuelidate';
import Notifications from 'vue-notification';

import App from './App.vue';
import router from './router';
import store from './store/index';

Vue.use(VueInputMask);
Vue.use(Vuelidate);
Vue.use(Notifications);

Vue.config.productionTip = false;

const vm = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

export default vm;

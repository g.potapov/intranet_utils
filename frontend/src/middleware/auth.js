import Cookies from 'js-cookie';

export default async function ({$store, next, router}) {
    const { token, role_id } = Cookies.getJSON('connect') || { token: '', role_id: null };

    if (token && role_id) {
        $store.commit('setToken', token);
        $store.commit('setRoleId', role_id);

        return next();
    } else if ($store.getters.isAuth) {
        return next();
    } else {
        return next({name: 'Auth'});
    }
}
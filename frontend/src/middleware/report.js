import Cookies from 'js-cookie';

export default async function ({ $store, next }) {
  const { token, roleId } = Cookies.getJSON('connect') || { token: '', roleid: null };

  if (token && roleId) {
    $store.commit('setToken', token);
    $store.commit('setRoleId', roleId);

    return next({ name: 'Report' });
  }

  return next();
}

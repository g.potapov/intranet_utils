export default function ({$store, next, router}) {
  if ($store.getters.isAdmin) {
    return next();
  }

  return next({name: 'Admin'});
}
export default async function ({$store, next, router}) {
  await $store.dispatch('oneUpdateReports');
  let isChecked = await $store.dispatch('checkFilesReady');

  if (!isChecked) {
    await $store.dispatch('updateReports');
  }

  return next();
}
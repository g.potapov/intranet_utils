import Vue from 'vue';
import Router from 'vue-router';
import $store from './store/index';

// Views
import Auth from './views/Auth.vue';
import Report from './views/Report.vue';
import Tp from './views/tp.vue';
import Company from './views/company.vue';
import medicsFull from './views/medicsFull.vue';
import viewsDeviations from './views/viewsDeviations.vue';
import avgMedReaction from './views/avgMedReaction.vue';
import rejects from './views/rejects.vue';
import medicsSmall from './views/medicsSmall.vue';
import viewsPerHour from './views/viewsPerHour.vue';
import admin from './views/admin.vue';
import Page404 from './views/404.vue';

// Middlewares
import auth from './middleware/auth';
import report from './middleware/report';
import isAdmin from './middleware/admin';
import updateReports from './middleware/updateReports';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/reports/',
  routes: [
    {
      path: '*',
      component: Page404,
    },
    {
      path: '/',
      name: 'Auth',
      component: Auth,
      meta: {
        middleware: report,
      },
    },
    {
      path: '/admin',
      name: 'Admin',
      component: admin,
      meta: {
        middleware: [auth, isAdmin],
      },
    },
    {
      path: '/report',
      name: 'Report',
      component: Report,
      meta: {
        middleware: auth,
      },
      children: [
        {
          path: '/report/tp',
          name: 'tpStatsViews',
          component: Tp,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/company',
          name: 'companyStats',
          component: Company,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/medicsfull',
          name: 'medicsFull',
          component: medicsFull,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/deviations',
          name: 'viewsDeviations',
          component: viewsDeviations,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/avgmedreaction',
          name: 'avgMedReaction',
          component: avgMedReaction,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/rejects',
          name: 'rejects',
          component: rejects,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/medicssmall',
          name: 'medicsSmall',
          component: medicsSmall,
          meta: {
            middleware: [auth, updateReports],
          },
        },
        {
          path: '/report/perhour',
          name: 'viewsPerHour',
          component: viewsPerHour,
          meta: {
            middleware: [auth, updateReports],
          },
        },
      ],
    },
  ],
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];

  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    context.next(...parameters);
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const context = {
      $store,
      from,
      next,
      router,
      to,
    };

    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware });
  }

  return next();
});

export default router;

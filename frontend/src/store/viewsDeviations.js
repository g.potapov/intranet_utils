// import api from '../api/index';
import moment from 'moment';

const state = {
  request: {
    startDate: '',
    finishDate: '',
    startTime: '',
    finishTime: '',
  },
  files: [],
};

const getters = {
  getRequest: state => state.request,
  getStartDate: state => moment(state.request.startDate, 'YYYY-MM-DD').format('DD.MM.YYYY'),
  getFinishDate: state => moment(state.request.finishDate, 'YYYY-MM-DD').format('DD.MM.YYYY'),
  getFiles: state => state.files,

  getStartTime: state => moment(state.request.startTime, 'HH:mm').format('HH:mm:ss'),
  getFinishTime: state => moment(state.request.finishTime, 'HH:mm').format('HH:mm:ss'),
};

/* eslint no-param-reassign: ["error", {
    "props": true,
    "ignorePropertyModificationsFor": ["state"]
  }] */
const mutations = {
  setStartDate: (state, date) => {
    state.request.startDate = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
  },

  setFinishDate: (state, date) => {
    state.request.finishDate = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
  },

  setFiles: (state, files) => {
    state.files = files;
  },

  setStartTime: (state, time) => state.request.startTime = moment(time, 'HH:mm:ss').format('HH:mm'),

  setFinishTime: (state, time) => state.request.finishTime = moment(time, 'HH:mm:ss').format('HH:mm'),
};

export default {
  state,
  getters,
  mutations,
  namespaced: true,
};

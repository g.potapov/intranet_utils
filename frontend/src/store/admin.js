import api from '../api/index';
import moment from 'moment';

const state = {
  loginUser: '',
  passwordUser: '',
  roleIdUser: 1
};

const getters = {
  getLoginUser: state => state.loginUser,
  getPasswordUser: state => state.passwordUser,
  getRoleIdUser: state => state.roleIdUser,
};

/* eslint no-param-reassign: ["error", {
    "props": true,
    "ignorePropertyModificationsFor": ["state"]
  }] */
const mutations = {
  setLoginUser: (state, login) => state.loginUser = login,
  setPasswordUser: (state, password) => state.passwordUser = password,
  setRoleIdUser: (state, id) => state.roleIdUser = id,
  resetDataAddUser: (state) => {
    state.loginUser = '';
    state.passwordUser = '';
    state.roleIdUser = 1;
  },
};

const actions = {
    addUser({ getters, rootGetters }) {
        return api({
            url: 'admin',
            method: 'post',
            data: {
                login: getters.getLoginUser,
                password: getters.getPasswordUser,
                new_role_id: getters.getRoleIdUser,
                token: rootGetters.getToken,
            }
        });
    }
};

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
};

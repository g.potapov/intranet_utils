/* eslint no-underscore-dangle: [
    "error", {
      "allow": ["_vm"],
    },
  ],
*/

import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';
import tpStatsViews from './tp';
import companyStats from './company';
import medicsFull from './medicsFull';
import viewsDeviations from './viewsDeviations';
import avgMedReaction from './avgMedReaction';
import rejects from './rejects';
import medicsSmall from './medicsSmall';
import viewsPerHour from './viewsPerHour';
import admin from './admin';
import api from '../api/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    auth: {
      client_id: null,
      role_id: null,
    },
    login: '',
    password: '',
    reportName: '',
    token: '',
    timeId: null,
    isResetTime: false,
    fileServer: '',
    regions: [
      {
        id: 'moscow',
        name: 'Москва'
      },
      {
        id: 'tver',
        name: 'Тверь'
      },
    ],
  },

  getters: {
    isAdmin: state => state.auth.role_id === 2,

    isAuth: state => state.token && state.auth.role_id,

    getClientId: state => state.auth.client_id,

    getReportName: state => state.reportName,

    getLogin: state => state.login,

    getPassword: state => state.password,

    getToken: state => state.token,

    getTimeId: state => state.timeId,

    isResetTime: state => state.isResetTime,

    getFileServer: state => state.fileServer,

    isLoadedFiles: (state, getters) => {
      let allFiles = getters[`${getters.getReportName}/getFiles`]
      let notReadyFiles = allFiles && allFiles.length !== 0
        ? allFiles.filter(file => !file.isReady && file.message !== 'внутренняя ошибка')
        : [];

      return getters.getTimeId && notReadyFiles.length !== 0;
    },

    getRegions: state => state.regions,
  },

  /* eslint no-param-reassign: ["error", {
    "props": true,
    "ignorePropertyModificationsFor": ["state"]
  }] */
  mutations: {
    setReportName: (state, name) => {
      state.reportName = name;
    },

    setClientId: (state, id) => {
      state.auth.client_id = id;
    },

    setRoleId: (state, id) => {
      state.auth.role_id = id;
    },

    setLogin: (state, login) => {
      state.login = login;
    },

    setPassword: (state, password) => {
      state.password = password;
    },

    setToken: (state, token) => {
      state.token = token;
    },

    setTimeId: (state, timeId) => {
      state.timeId = timeId;
    },

    setResetTime: (state, bool) => state.isResetTime = bool,
  },

  actions: {
    resetTimeId({ dispatch }) {
      dispatch('updateReports', true);
    },

    async auth({ commit, getters }) {
      return new Promise(async (resolve, reject) => {
        const {
          data: {
            token,
            role_id: roleId,
          },
        } = await api({
          url: 'auth',
          method: 'post',
          data: {
            login: getters.getLogin,
            password: getters.getPassword,
          },
        }).catch((error) => {
          const {
            response: {
              status,
            } = {respones: {status: null}},
          } = error;

          if (status === 500) {
            this._vm.$notify({
              group: 'note',
              type: 'error',
              title: 'Ошибка',
              text: 'Не верный логин или пароль.',
            });
          }

          if (!status) {
            this._vm.$notify({
              group: 'note',
              type: 'error',
              title: 'Ошибка',
              text: 'Сервис не доступен.',
            });
          }
        }) || { data: { client_id: undefined, role_id: undefined } };

        if (token && roleId) {
          Cookies.set(
            'connect',
            { token, roleId },
            { expires: 7, path: '/' },
          );
          commit('setToken', token);
          commit('setRoleId', roleId);
          return resolve();
        }

        return reject();
      });
    },

    async exit({ commit }) {
      Cookies.remove('connect');

      commit('setToken', '');
      commit('setRoleId', null);
      commit('setLogin', '');
      commit('setPassword', '');
      commit('setReportName', '');

      await api({
        url: 'exit',
        method: 'post',
      });
    },

    async addReport({ getters, dispatch }) {
      await dispatch('resetTimeId');
      await api({
        url: 'add',
        method: 'post',
        data: {
          ...getters[`${getters.getReportName}/getRequest`],
          reportName: getters.getReportName,
          token: getters.getToken,
        },
      });

      await dispatch('updateReports');
    },

    async checkFilesReady({ getters, dispatch }) {
      return new Promise((resolve, reject) => {
        let allFiles = this.getters[`${getters.getReportName}/getFiles`]
        let notReadyFiles = allFiles && allFiles.length !== 0
          ? allFiles.filter(file => !file.isReady && file.message !== 'внутренняя ошибка')
          : [];

        if (notReadyFiles.length === 0) {
          return resolve(true);
        }

        return resolve(false);
      });
    },

    async oneUpdateReports({ getters, commit, dispatch }) {
      let {data: {
        statuses
      }} = await api({
        url: 'updateReports',
        method: 'post',
        data: {
          token: getters.getToken,
          reportName: getters.getReportName,
        },
      }) || {data: {statuses: []}};

      if (statuses && statuses.length !== 0) {
        commit(`${getters.getReportName}/setFiles`, statuses);
      }
    },

    async updateReports({ getters, dispatch, commit }, isReset = false) {
      if (isReset) {
        clearTimeout(getters.getTimeId);
        commit('setTimeId', null);
      } else {
        await dispatch('oneUpdateReports');
        let isChecked = await dispatch('checkFilesReady');

        commit('setTimeId', setTimeout(async () => {
          await dispatch('oneUpdateReports');

          isChecked
            ? await dispatch('updateReports', true)
            : await dispatch('updateReports');
          }, 5000)
        );
      }
    },
  },
  modules: {
    tpStatsViews,
    companyStats,
    medicsFull,
    viewsDeviations,
    avgMedReaction,
    rejects,
    medicsSmall,
    viewsPerHour,
    admin,
  },
});

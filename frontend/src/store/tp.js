// import api from '../api/index';
import moment from 'moment';

const state = {
  request: {
    startDate: '',
    finishDate: '',
    startTime: '',
    finishTime: '',
  },
  files: [],
};

const getters = {
  getRequest: state => state.request,
  getStartDate: state => moment(state.request.startDate, 'YYYY-MM-DD').format('DD.MM.YYYY'),
  getFinishDate: state => moment(state.request.finishDate, 'YYYY-MM-DD').format('DD.MM.YYYY'),
  getFiles: state => state.files,
};

/* eslint no-param-reassign: ["error", {
    "props": true,
    "ignorePropertyModificationsFor": ["state"]
  }] */
const mutations = {
  setStartDate: (state, date) => {
    state.request.startDate = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
  },
  setFinishDate: (state, date) => {
    state.request.finishDate = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
  },

  setFiles: (state, files) => {
    state.files = files;
  },
};

const actions = {

};

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
};

import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '../vuetify-style/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#2a4d7d',
    secondary: '#2a4d7d',
  },
});

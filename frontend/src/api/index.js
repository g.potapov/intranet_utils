import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.NODE_ENV === 'development'
    // ? 'http://localhost:3086/reports'
    ? 'https://preprod.medpoint24.ru:5001/reports'
    : window.config.apiUrl,
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json'
  },
});

instance.interceptors.response.use(
  response => response,
  error => Promise.reject(error),
);

export default instance;

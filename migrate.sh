#!/bin/bash

if [ "$1" == "create" ]; then
exec node node_modules/db-migrate/bin/db-migrate $@ --config $(pwd)/config/database.json --sql-file --env pg
else
exec node node_modules/db-migrate/bin/db-migrate $@ --config $(pwd)/config/database.json --env pg 
fi

require('dotenv').config();

const useCaseDymov = require('../reportMailing/useCaseDymov');
const useCaseViews = require('../reportMailing/useCaseViews');

Promise.all([
    useCaseDymov(),
    useCaseViews(),
]);

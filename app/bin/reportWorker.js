require('dotenv').config();

const logic = require('../reports/serviceLogic/reportWorkerLogic');

interval(logic.startWorkerQuery);

function interval(cb, delay = 10 * 1000) {
    setTimeout(async function task() {
        try {
            await cb();
            setTimeout(task, delay);
        }
        catch (err) {
            console.log(err.message);
        }
    }, delay);
}

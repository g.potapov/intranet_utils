const rp = require('request-promise');
const papa = require('papaparse');
const { Client } = require('pg');
const lodash = require('lodash');
const config = require('../../config');
const fs = require('fs');
const util = require('util');


/// Spreadsheets sheets
const ORGANIZATIONS = 'organizations';
const MEDICS = 'medics';

/// Spreadsheets columns
const ORG_ID = 'orgId';
const MEDIC_ID = 'medicId';
const NAME = 'name';
const PROFILE = 'profile';

/// Special values
const ALL = 'all';
const IGNORE = 'ignore';

const promiseWriteFile = util.promisify(fs.writeFile);

async function writeDataToDisk(data) {

    const id = Date.now();
    await promiseWriteFile(`${config.workDirMedicsByArea}/${id}`, JSON.stringify(data));

    return id;
}

async function loadDataFromDisk(pid) {
    const promiseReadFile = util.promisify(fs.readFile);
    const bufferData = await promiseReadFile(`${config.workDirMedicsByArea}/${pid}`);
    return JSON.parse(bufferData.toString());
}


async function getDataGoogleDocs(sheetType) {
    const url = `https://docs.google.com/spreadsheets/d/${config.medicOrgsSpreadSheetId}/gviz/tq?tqx=out:csv&sheet=${sheetType}`;
    const csv = await rp(url);

    return papa.parse(csv, { header: true }).data;
}

function aggregateOrgData(rawData, dbOrgs, errors) {
    const res = { [ALL]: [] };

    rawData.forEach(obj => {
        const id = parseInt(obj[ORG_ID]);
        const towns = obj[PROFILE];

        if (!dbOrgs[id]) {
            errors.push('В базе отсутствует организация ' + id + ' ' + obj[NAME] + ', пропускаем');
            return;
        }

        towns.split('+').forEach(town => {
            if (!res[town])
                res[town] = [];

            res[town].push(id);
        });

        res[ALL].push(id);
    });

    return res;
}


function aggregateMedicData(rawData, orgsData, errors) {
    const data = {};
    const names = {};

    rawData.forEach(obj => {
        const id = parseInt(obj[MEDIC_ID]);
        const town = obj[PROFILE];
        if (!town || town === IGNORE) return;

        let orgsList = [];
        town.split('+').forEach(town => {
            if (!orgsData[town]) {
                errors.push(`Пустая территория у медика ${id}: ${town}, пропускаем`);
                return;
            }

            orgsList = orgsList.concat(orgsData[town]);
        });

        data[id] = orgsList;
        names[id] = obj[NAME];
    });

    return {
        data,
        names
    };
}



async function connectToDb(cb) {
    const client = new Client({
        connectionString: process.env.MEDIST_DB
    });

    await client.connect();

    const res = await cb(client);

    await client.end();

    return res;
}



const index = async (req, res) => {
    const [rawOrgsData, rawMedicsData] = await Promise.all([
        getDataGoogleDocs(ORGANIZATIONS),
        getDataGoogleDocs(MEDICS),
    ]);


    const { rows: dbOrgsRows } = await connectToDb(client => {
        return client.query('select id from drivers_organization');
    });
    const dbOrgs = {};
    dbOrgsRows.map(({ id }) => {
        dbOrgs[id] = true
    });

    const errors = [];

    const orgsData = aggregateOrgData(rawOrgsData, dbOrgs, errors);
    const { data: medicData, names: medicNames } = aggregateMedicData(rawMedicsData, orgsData, errors);


    const { rows: dbData } = await connectToDb(client => {
        return client.query(`select uu.id, uu.role_id, array_agg(uuo.org_id) as orgs from users_users uu 
                          left join users_usersorg uuo on (uuo.user_id = uu.id) 
                          where role_id in (2, 4, 10, 12) group by uu.id order by 1;`);
    });

    const dbMedicData = {};
    dbData.forEach(({ id, orgs }) => {
        dbMedicData[id] = orgs.filter(v => v);
    });

    const medicOrgsMutations = {};
    Object.keys(medicData).forEach(id => {
        const medicOrgs = medicData[id];
        const dbMedicOrgs = dbMedicData[id];

        if (!dbMedicOrgs) {
            errors.push(`Медик ${id} ${medicNames[id]} отсутствует в базе, пропускаем`);
            return;
        }

        medicOrgsMutations[id] = {
            add: lodash.difference(medicOrgs, dbMedicOrgs),
            remove: lodash.difference(dbMedicOrgs, medicOrgs)
        };
    });

    //console.log(errors);
    ///console.log(medicNames);
    const isMedicOrgsMutationsEmpty = Object.values(medicOrgsMutations)
        .every(({ add, remove }) => (add.length === 0 && remove.length === 0));
    let pid = 0;

    if (!isMedicOrgsMutationsEmpty)
        pid = await writeDataToDisk(medicOrgsMutations);

    res.render('medicsByArea/index', { errors, medicOrgsMutations, medicNames, pid });
};


const delay = (time) => {
    return new Promise(resolve => {
        setTimeout(resolve, time);
    });
};


const processing = async function (req, res) {
    const data = await loadDataFromDisk(req.body.pid);
    ///console.log(data);

    await connectToDb(async client => {
        await Promise.all(
            Object.entries(data)
                .map(async ([medicId, { add, remove }]) => {
                    if (add.length) {
                        const addQuery = 'insert into users_usersorg(user_id, org_id) values ' + add.map(orgId => `(${medicId}, ${orgId})`).join(',');
                        console.log(addQuery);
                        await client.query(addQuery);
                    }

                    if (remove.length) {
                        const removeQuery = `delete from users_usersorg where user_id=${medicId} AND org_id IN (${remove.join(',')})`;
                        console.log(removeQuery);
                        await client.query(removeQuery);
                    }

                    await delay(300);    /// FIXME maybe useless
                })
        );
    });

    res.redirect('/medicsByArea');
};

module.exports = {
    index,
    processing
};
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

const cors = require('cors')

var app = express();

app.use(cors());

// view engine setup
app.set('views', 'app/views');
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('/public'));

app.use((req, res, next) => {
  console.log(`incoming request: path - ${req.path}, method - ${req.method}, body - `, req.body);

  next();
});

require('./routes')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {

  // render the error page
  res.status(200).send({
    status: err.status,
    message: err.message
  });

});

module.exports = app;

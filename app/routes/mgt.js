const router = require('express').Router();
const mgtController = require('../mgt/controllerMgt');

router.post('/', mgtController.getMgtStatistics);

router.get('/organizations', mgtController.getOrganizations);

module.exports = router;
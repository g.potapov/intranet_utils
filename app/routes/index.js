const medicsByAreaController = require('../medicsByArea/controller');
const reportsRouter = require('./reports');
const mgtRouter = require('./mgt');

const path = require('path');

module.exports = function (app) {
    app.get('/', function (req, res) {
        res.sendFile(path.resolve(__dirname, '../../frontend/public/index.dev.html'));
    });

    app.get('/medicsByArea', medicsByAreaController.index);
    app.post('/medicsByArea', medicsByAreaController.processing);

    app.use('/reports', reportsRouter);

    app.use('/mgt', mgtRouter);
};
const router = require('express').Router();
const reportsController = require('../reports/controller');

const { isAdminAuth, isUserAuth } = require('../middlwares/authSession');

router.post('/auth', reportsController.postAuth);

router.post('/admin', isAdminAuth, reportsController.addUser);

router.post('/add', isUserAuth, reportsController.addReport);

router.get('/exit', isUserAuth, reportsController.exit);

router.post('/updateReports', isUserAuth, reportsController.updateReports);

module.exports = router;


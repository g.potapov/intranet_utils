
const {
    addReportToProcessing,
    checkAuth,
    addNewUser,
    checkLogin,
    getReportStatuses } = require('./serviceLogic/reportAccessLogic');

const utils = require('../utils');

const { createToken } = require('../middlwares/authSession');

async function addReport(req, res, next) {
    const { reportName, client_id, ...reportParams } = req.body;

    try {
        validateReportParams(reportParams);

        await addReportToProcessing(reportParams, reportName, client_id);

        res.status(200).send();
    }
    catch (err) {
        console.log(err);
        let message = 'Ошибка добавления отчета в очередь сервера, обратитесь к разработчикам';
        let status = 500

        if (err instanceof utils.MyError) {
            message = err.message;
            status = 400;
        }

        res.status(status).send({
            errMsg: message
        });
    }
}

async function postAuth(req, res, next) {

    try {
        const { client_id, role_id } = await checkAuth(req.body);

        const token = await createToken({ client_id, role_id });

        res.status(200).send({
            token,
            role_id
        });
    }
    catch (err) {
        console.log(err);
        res.status(500).send({
            errMsg: 'при авторизации произошла ошибка, обратитесь к разработчикам'
        });
    }
}

async function addUser(req, res, next) {
    try {
        await validateUserData(req.body);

        await addNewUser(req.body);

        res.status(200).send();
    }
    catch (err) {
        console.log(err);
        const status = err.status ? err.status : 500;
        const errMsg = err.status ? err.message : 'ошибка при добавлении пользвателя, обратитесь к разработчикам'

        res.status(status).send({
            errMsg
        });
    }
}

function exit(req, res, next) {
    req.status(200).send();
}

async function validateUserData(params) {

    const { login, password } = params;

    if (!login || !password) {
        const err = new Error('пароль или логин не может быть пустым!');
        err.status = 400;
        throw err;
    }

    if (password.length < 6 || password.length > 20) {
        const err = new Error('пароль должен быть не менее 6 и не более 20 симоволов');
        err.status = 400;
        throw err;
    }

    if (/^[.@_a-zA-Z0-9]+$/.test(login) === false) {
        const err = new Error('В логине должны быть только латинские буквы и спец символы @ . _');
        err.status = 400;
        throw err;
    }

    if (login.length < 4 || login.length > 50) {
        const err = new Error('В логине должен быть от 4 до 50 символов');
        err.status = 400;
        throw err;
    }

    if (await checkLogin(login)) {
        const err = new Error('Пользователь с таким именем уже существует!');
        err.status = 400;
        throw err;
    }
}

async function updateReports(req, res, next) {
    const { client_id, reportName } = req.body;
    try {
        const statuses = await getReportStatuses(client_id, reportName);

        res.status(200).send({
            statuses
        });
    }
    catch (err) {
        console.log(err.message);
        res.status(500).send({
            errMsg: 'ошибка получения статусов отчетов, обратитесь к разработчикам'
        });
    }
}

function validateReportParams(params) {

    const { startDate, finishDate, startTime, finishTime } = params;

    if (startDate)
        utils.validateDate(startDate);

    if (finishDate)
        utils.validateDate(finishDate);

    if (startTime)
        utils.validateTime(startTime);

    if (finishTime)
        utils.validateTime(finishTime);

    if (startTime)
        utils.validateIntervalDatetime(startDate, startTime, finishDate, finishTime);
    else
        utils.validateIntervalDate(startDate, finishDate);
}



module.exports = {
    addReport,
    postAuth,
    updateReports,
    addUser,
    exit,
    //updateReports,
}
const daoPg = require('../../dao/daoPg');

const utils = require('../../utils');
const dbHelp = require('../../utils/dbHelp');

const constants = require('../../utils/constants');

function addReportToProcessing(reportParams, reportName, clientId) {

    const { token, ...params } = reportParams;

    return daoPg.saveReportRequestToBd(JSON.stringify(params), reportName, clientId);
}

function setReportState(id, status) {
    return daoPg.updateReportStatus(id, status);
}

function execReport(query) {

    return daoPg.executeReportsQuery(query);
}

function loadReports() {
    return daoPg.getReportsRowsFromBd();
}

function saveReport(headers, rawData, path, name, id) {

    return utils.createCSVFile(headers, rawData, path, name, id);
}

async function checkAuth(params) {
    const { login, password } = params;

    const clientData = await daoPg.getClientData(login);

    if (!clientData)
        throw new Error('неверное имя пользователя или пароль');

    const compareResult = dbHelp.comparePassword({
        password: password,
        salt: clientData.salt,
        hashedPassword: clientData.hashed_pass
    });

    if (!compareResult)
        throw new Error('неверное имя пользователя или пароль');


    return {
        client_id: clientData.client_id,
        role_id: clientData.role_id
    }
}

function addNewUser(params) {

    const { login, password, new_role_id } = params;

    const { salt, hashedPassword } = dbHelp.generateSaltAndHashedPassword(password);

    const bdData = {
        salt,
        hashedPassword,
        login,
        new_role_id
    };

    return daoPg.saveUserInDb(bdData);
}

async function checkLogin(login) {

    const count = await daoPg.checkIfClientExistByLogin(login);

    return !!count;
}

async function getReportStatuses(clientId, reportName) {

    const statusesFromDb = await daoPg.getReportStatusesFromBd(clientId, reportName);

    if (statusesFromDb.length === 0)
        return;

    const results = statusesFromDb.map(({ id, creation_date, status }) => {

        let message;
        const downloadLink = `/files/${reportName}_${id}.csv`;
        const name = `${reportName}_${id}.csv`;

        switch (status) {
            case constants.WORK_STATUS.FREE: message = 'ожидает обработку'; break;
            case constants.WORK_STATUS.BUSY: message = 'обрабатывается'; break;
            case constants.WORK_STATUS.FAIL: message = 'внутренняя ошибка'; break;
            case constants.WORK_STATUS.DONE: message = creation_date; break;
        }

        return {
            isReady: status === constants.WORK_STATUS.DONE,
            message,
            name,
            downloadLink
        };
    });

    return results;
}

module.exports = {
    addReportToProcessing,
    execReport,
    loadReports,
    setReportState,
    saveReport,
    checkAuth,
    addNewUser,
    checkLogin,
    getReportStatuses
}
const constants = require('../../utils/constants');

const {
    loadReports,
    execReport,
    saveReport,
    setReportState
} = require('../serviceLogic/reportAccessLogic');

const config = require('../../../config');

const { getReportQuery, postProcessingHandler, getReportHeaders } = require('./reports');

async function startWorkerQuery() {
    let reportsQueries = await loadReports();

    reportsQueries = reportsQueries.map(({ id, query, name }) =>
        ({
            id: id,
            query: getReportQuery(name, JSON.parse(query)),
            name: name
        })
    );

    if (reportsQueries.length === 0)
        return;

    const reportTasks = reportsQueries.map(({ id, query, name }) => createTask(id, query, name));

    await Promise.all(reportTasks);
};

async function createTask(id, query, name) {
    let status;

    try {
        const label = `sql query for ${name}, id - ${id}, process time: `;

        console.time(label);
        let rawCsv = await execReport(query);
        console.timeEnd(label);

        rawCsv = postProcessingHandler(name, rawCsv);

        await saveReport(getReportHeaders(name), rawCsv, config.workDirReports, name, id);

        status = constants.WORK_STATUS.DONE;
    }
    catch (err) {
        status = constants.WORK_STATUS.FAIL;
        console.log(`error - ${err.message}, stack - ${err.stack}`);
    }
    finally {
        return setReportState(id, status);
    }
}

module.exports = {
    startWorkerQuery,
}


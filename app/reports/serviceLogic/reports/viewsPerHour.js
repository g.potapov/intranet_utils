const { concatDateAndTime } = require('../../../utils/dbHelp');

const constants = require('../../../utils/constants');

const headers = {
    hour: "Час",
    viewCount: "Количество осмотров",
}

function getQuery(params) {

    const { startDate, startTime, finishDate, finishTime, region } = params;

    const group_medic = (region === constants.REGIONS.MOSCOW) ? '(1)' : '(2)';
    const start = concatDateAndTime(startDate, startTime);
    const finish = concatDateAndTime(finishDate, finishTime);

    const query = {
        values: [start, finish],

        text: `SELECT to_char(dmd.date, 'HH24') as "${headers.hour}", 
                     COUNT(to_char(dmd.date, 'HH24')) as "${headers.viewCount}"
                FROM drivers_meddata dmd
                JOIN drivers_medresult dmr ON dmd.id=dmr."medData_id"
                JOIN drivers_medics dm ON dm.id=dmr."medUser_id"
                WHERE (dmd.date BETWEEN $1::timestamptz AND $2::timestamptz) AND dm.group_medic IN ${group_medic}
    
                GROUP BY "${headers.hour}"
                ORDER BY "${headers.hour}";`
    };

    return query;
}

function postProcessingHandler(rawData) {

    rawData.forEach(raw => {

        raw[headers.hour] = +raw[headers.hour] + 1;
    });

    return rawData;
}

function getHeaders() {
    return [
        headers.hour,
        headers.viewCount
    ];
}

module.exports = {
    getQuery,
    postProcessingHandler,
    getHeaders
}
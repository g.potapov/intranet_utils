
const headers = {
    date: "Дата",
    avgLoad: "Среднее время загрузки видео",
    avgOpen: "Среднее время на открытие осмотра",
    avgDecision: "Среднее время на принятие решения"
};

function getQuery(params) {

    const { startDate, finishDate } = params;

    const query = {
        values: [startDate, finishDate],
        text: `SELECT to_char(dmd.date, 'YYYY-MM-DD') as "${headers.date}",
                        TO_CHAR( CAST (AVG(EXTRACT(epoch FROM uv.date - dmd.date))::text AS interval), 'HH24:MI:SS') AS "${headers.avgLoad}",
                        TO_CHAR( CAST (AVG(EXTRACT(epoch FROM dd2.date - dd1.date)) as text)::interval, 'HH24:MI:SS') AS "${headers.avgOpen}",
                        TO_CHAR( CAST (AVG(EXTRACT(epoch FROM dd3.date - dd2.date)) as text)::interval, 'HH24:MI:SS') AS "${headers.avgDecision}"
                FROM drivers_meddata dmd
                        JOIN uploadapp_video_data uv ON dmd.id = uv.med_data
                        join drivers_meddatalog dd1 on dd1."medData_id"=dmd.id
                        join drivers_meddatalog dd2 on dd2."medData_id"=dmd.id
                        join drivers_meddatalog dd3 on dd3."medData_id"=dmd.id
                        join drivers_medics dm on dd1."medUser_id"=dm.id OR dd2."medUser_id"=dm.id OR dd3."medUser_id"=dm.id
                    WHERE dmd.date BETWEEN $1::timestamptz AND $2::timestamptz and dd1."ActionState"=2 and dd2."ActionState"=4 and dd3."ActionState"=5 AND dm.group_medic=1

                    GROUP BY "${headers.date}"
                    ORDER BY "${headers.date}";`
    };

    return query;
}

function getHeaders() {
    return [
        headers.date,
        headers.avgLoad,
        headers.avgOpen,
        headers.avgDecision
    ];
}

module.exports = {
    getQuery,
    getHeaders
}
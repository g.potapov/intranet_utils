const { concatDateAndTime } = require('../../../utils/dbHelp');

const constants = require('../../../utils/constants');

const headers = {
    period: "Период",
    id: "id",
    medFio: "ФИО медработника",
    viewsCount: "Количество осмотров",
};

function getQuery(params) {

    const { startDate, startTime, finishDate, finishTime, region } = params;

    const group_medic = (region === constants.REGIONS.MOSCOW) ? '(1)' : '(2)';

    const start = concatDateAndTime(startDate, startTime);
    const finish = concatDateAndTime(finishDate, finishTime);

    const query = {
        values: [start, finish],
        text: `SELECT TO_CHAR($1::timestamptz, 'YYYY-MM-DD')||' - '||TO_CHAR($2::timestamptz, 'YYYY-MM-DD') as "${headers.period}",
                    dm.id as "${headers.id}",
                    dm.surname||' '||dm.name||' '||dm.patronymic as "${headers.medFio}",
                    COUNT(dmr."medData_id") as "${headers.viewsCount}"
                FROM drivers_medics dm 
                    JOIN drivers_medresult dmr ON dmr."medUser_id"=dm.id
                    JOIN drivers_meddata dmd ON dmd.id=dmr."medData_id"
                    WHERE dmd.date BETWEEN $1::timestamptz AND $2::timestamptz AND dm.group_medic=${group_medic}

                    GROUP BY dm.id
                    ORDER BY "${headers.medFio}";`
    };

    return query;
}


function getHeaders() {
    return [
        headers.period,
        headers.id,
        headers.medFio,
        headers.viewsCount,
    ];
}

module.exports = {
    getQuery,
    getHeaders
}
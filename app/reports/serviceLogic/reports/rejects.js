const headers = {
    date: "Дата",
    totalCount: "Общее число осмотров",
    mpReject: "Недопуск по МП",
    percentMp: "% мп",
    admReject: "Недопуск по АДМ",
    percentAdm: "% адм",
    totalReject: "Общее число недопусков",
    percentTotal: "% общий"
};

function getQuery(params) {

    const { startDate, finishDate } = params;

    const query = {
        values: [startDate, finishDate],
        text: `SELECT  tbl.dt as "${headers.date}",
                    tbl.total as "${headers.totalCount}",
                    tbl.mp "${headers.mpReject}",
                    round(cast(tbl.mp as numeric)/cast(tbl.total as numeric)*100, 2) as "${headers.percentMp}",
                    tbl.adm "${headers.admReject}",
                    round(cast(tbl.adm as numeric)/cast(tbl.total as numeric)*100, 2) as "${headers.percentAdm}",
                    round(cast (mp as numeric) + cast(adm as numeric), 2) as "${headers.totalReject}",
                    round((cast (mp as numeric) + cast(adm as numeric))/cast(tbl.total as numeric)*100, 2) as "${headers.percentTotal}"
                 FROM (SELECT to_char(dmd.date, 'YYYY-MM-DD') as dt,
                            COUNT(dmd.id) as total,

                            COUNT(case when (dmr.comment SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%' 
                                OR (dmr.comment SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%' AND dmr.comment SIMILAR TO '%(вне зоны|убор/очки/верхнюю|Сбой показателей|Нарушение|не загружается)%'))
                                 AND dmr."medResult"=FALSE then 1 end) as mp,

                            COUNT(case when dmr.comment NOT SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%'
                                AND dmr."comment" SIMILAR TO '%(вне зоны|убор/очки/верхнюю|Сбой показателей|Нарушение|не загружается)%' AND dmr."medResult"=FALSE then 1 end) as adm

                                FROM drivers_meddata dmd
                                JOIN drivers_medresult dmr ON dmr."medData_id"=dmd.id
                                JOIN drivers_medics dm ON dm.id=dmr."medUser_id"
                            WHERE dmd.date BETWEEN $1::timestamptz AND $2::timestamptz AND dm.group_medic=1
                GROUP BY dt
                ORDER BY dt) tbl;`
    };

    return query;
}


function getHeaders() {
    return [
        headers.date,
        headers.totalCount,
        headers.mpReject,
        headers.percentMp,
        headers.admReject,
        headers.percentAdm,
        headers.totalReject,
        headers.percentTotal
    ];
}

module.exports = {
    getQuery,
    getHeaders
}
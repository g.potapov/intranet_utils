const companyStats = require('./companyStats');
const tpStatsViews = require('./tpStatsViews');
const medicsFull = require('./medicsFull');
const viewsDeviations = require('./viewsDeviations');
const avgMedReaction = require('./avgMedReaction');
const rejects = require('./rejects');
const medicsSmall = require('./medicsSmall');
const viewsPerHour = require('./viewsPerHour');


const reportMapping = {
    'tpStatsViews': tpStatsViews,
    'companyStats': companyStats,
    'medicsFull': medicsFull,
    'viewsDeviations': viewsDeviations,
    'avgMedReaction': avgMedReaction,
    'rejects': rejects,
    'medicsSmall': medicsSmall,
    'viewsPerHour': viewsPerHour,
}

function getReportHeaders(reportName) {
    if (reportMapping[reportName]) {
        return reportMapping[reportName].getHeaders();
    }

    throw ('Отчета с таким именем не существует');
}

function getReportQuery(reportName, params) {

    if (reportMapping[reportName]) {
        return reportMapping[reportName].getQuery(params);
    }

    throw ('Отчета с таким именем не существует');
}

function postProcessingHandler(reportName, rawData) {

    if (reportMapping[reportName].postProcessingHandler) {
        return reportMapping[reportName].postProcessingHandler(rawData);
    }

    return rawData;
}

module.exports = {
    getReportQuery,
    postProcessingHandler,
    getReportHeaders
}
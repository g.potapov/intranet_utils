const headers = {
    number: "Номер",
    date: "Дата",
    viewType: "Тип осмотра",
    result: "Результат",
    driverFio: "ФИО",
    viewTime: "Время осмотра на терминале",
    videoLoadTime: "Загрузка видео",
    queryTime: "Попадание в очередь",
    medicTime: "Осмотр медиком",
    stickerTime: "Печать стикера",
    hostname: "hostname",
    orgId: "id организации",
    medFio: "Медик",
    comment: "Комментарий"
};

function getQuery(params) {

    const { startDate, finishDate } = params;

    const query = {
        values: [startDate, finishDate],
        text: `SELECT dmd.id as "${headers.number}",
                        to_char(l.dt, 'YYYY-MM-DD HH24:MI:SS') as "${headers.date}",
                        dmd.beforeafter as "${headers.viewType}",
                        dr."medResult" as "${headers.result}",
                        dd.surname||' '|| dd.name||' '|| dd.patronymic as "${headers.driverFio}",
                        TO_CHAR((dmd."dateEnd" - dmd."dateBegin"),'HH24:MI:SS') as "${headers.viewTime}",
                        '00:00:00' as "${headers.videoLoadTime}",
                        '00:00:00' as "${headers.queryTime}",
                        TO_CHAR((dr.date - l.dt),'HH24:MI:SS') as "${headers.medicTime}",
                        TO_CHAR(COALESCE((ut.date - dr.date),interval '0 sec'),'HH24:MI:SS') as "${headers.stickerTime}",
                        hostname as "${headers.hostname}", 
                        org_id as "${headers.orgId}", 
                        dm.surname||' '|| dm.name||' '|| dm.patronymic as "${headers.medFio}",
                        dr.comment as "${headers.comment}"
                    FROM drivers_meddata dmd
                        join drivers_medresult dr ON dmd.id = dr."medData_id"
                        join drivers_driver dd ON dmd.driver_id = dd.id
                        join drivers_medics dm on dm.id=dr."medUser_id"
                        join (select min(date) dt, "medData_id" from drivers_meddatalog where "ActionState"=6
                            and date BETWEEN $1::timestamptz AND $2::timestamptz group by "medData_id") l on dmd.id=l."medData_id"
                        left join stamp_history ut ON dr.id = ut.result_id
                UNION
                SELECT dmd.id as "${headers.number}",
                        to_char(l.dt, 'YYYY-MM-DD HH24:MI:SS') as "${headers.date}",
                        dmd.beforeafter as "${headers.viewType}",
                        dr."medResult" as "${headers.result}",
                        dd.surname||' '|| dd.name||' '|| dd.patronymic as "${headers.driverFio}",
                        TO_CHAR((dmd."dateEnd" - dmd."dateBegin"),'HH24:MI:SS') as "${headers.viewTime}",
                        TO_CHAR((uv.date - dmd.date),'HH24:MI:SS') as "${headers.videoLoadTime}",
                        TO_CHAR((l.dt - uv.date),'HH24:MI:SS') as "${headers.queryTime}",
                        TO_CHAR((dr.date - l.dt),'HH24:MI:SS') as "${headers.medicTime}",
                        TO_CHAR(COALESCE((ut.date - dr.date),interval '0 sec'),'HH24:MI:SS') as "${headers.stickerTime}",
                        hostname as "${headers.hostname}", 
                        org_id as "${headers.orgId}", 
                        dm.surname||' '|| dm.name||' '|| dm.patronymic as "${headers.medFio}",
                        dr.comment as "${headers.comment}"                
                    FROM drivers_meddata dmd
                        JOIN uploadapp_video_data uv ON dmd.id = uv.med_data
                        join drivers_medresult dr ON dmd.id = dr."medData_id"
                        join drivers_driver dd ON dmd.driver_id = dd.id
                        join drivers_medics dm on dm.id=dr."medUser_id"
                        join (select min(date) dt, "medData_id" from drivers_meddatalog where ("ActionState"=3 or "ActionState"=-2)
                            and date BETWEEN $1::timestamptz AND $2::timestamptz group by "medData_id") l on dmd.id=l."medData_id"
                        left join stamp_history ut ON dr.id = ut.result_id
                        ORDER BY "${headers.date}";`
    };

    return query;
}

function postProcessingHandler(rawData) {

    const inspectionTypes = {
        before: "предрейсовый",
        after: "послерейсовый",
        line: "линейный",
        beforeshift: "предсменный",
        aftershift: "послесменный",
        alcho: "контроль трезвости"
    }

    rawData.forEach(raw => {

        raw[headers.viewType] = inspectionTypes[raw[headers.viewType]];
        raw[headers.result] = raw[headers.result] ? 'пройден' : 'не пройден'
    });

    return rawData;
}


function getHeaders() {
    return [
        headers.number,
        headers.date,
        headers.viewType,
        headers.result,
        headers.driverFio,
        headers.viewTime,
        headers.videoLoadTime,
        headers.queryTime,
        headers.medicTime,
        headers.stickerTime,
        headers.hostname,
        headers.orgId,
        headers.medFio,
        headers.comment
    ];
}

module.exports = {
    getQuery,
    postProcessingHandler,
    getHeaders
}
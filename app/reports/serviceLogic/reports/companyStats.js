const headers = {
	period: "Период",
	id: "id",
	organization: "Организация",
	totalViews: "Всего осмотров",
	acceptedCount: "Допуск (количество осмотров)",
	rejectedCount: "Недопуск (количество осмотров)",
	mpReject: "Недопуск по МП",
	admReject: "Недопуск по АДМ",
};

function getQuery(params) {

	const { startDate, finishDate } = params;

	const query = {
		values: [startDate, finishDate],
		text: `SELECT TO_CHAR($1::timestamptz, 'YYYY-MM-DD')||' - '||TO_CHAR($2::timestamptz, 'YYYY-MM-DD') as "${headers.period}",
					dorg.id as "${headers.id}", 
					dorg.name as "${headers.organization}", 
					COUNT(dmr."medData_id") as "${headers.totalViews}",
					COUNT(case when dmr."medResult" = TRUE then 1 end) as "${headers.acceptedCount}",
					COUNT(case when dmr."medResult" = FALSE then 1 end) as "${headers.rejectedCount}",	
					COUNT(case when dmr."medResult" = FALSE AND dmr.comment SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%' 
						OR (dmr.comment SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%' AND dmr.comment SIMILAR TO '%(вне зоны|убор/очки/верхнюю|Сбой показателей|Нарушение|не загружается)%') then 1 end) as "${headers.mpReject}",
					COUNT(case when dmr."medResult" = FALSE AND dmr.comment NOT SIMILAR TO '%(АД|ЧСС|паров|покровов|жалоб)%'
						AND dmr."comment" SIMILAR TO '%(вне зоны|убор/очки/верхнюю|Сбой показателей|Нарушение|не загружается)%' then 1 end) as "${headers.admReject}"
				FROM drivers_organization dorg
					JOIN drivers_driver dd ON dd.org_id=dorg.id
					JOIN drivers_meddata dmd ON dd.id=dmd.driver_id
					JOIN drivers_medresult dmr ON dmr."medData_id"=dmd.id
					JOIN drivers_medics dm ON dm.id=dmr."medUser_id"
				WHERE dmd.date BETWEEN $1::timestamptz AND $2::timestamptz AND dm.group_medic=1
				GROUP BY dorg.id, dorg.name
				ORDER BY dorg.id;`
	};

	return query;
}

function getHeaders() {
	return [
		headers.period,
		headers.id,
		headers.organization,
		headers.totalViews,
		headers.acceptedCount,
		headers.rejectedCount,
		headers.mpReject,
		headers.admReject,
	];
}

module.exports = {
	getQuery,
	getHeaders
}
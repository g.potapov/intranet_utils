const headers = {
    period: "Период",
    id: "id",
    medFio: "ФИО медработника",
    viewsCount: "Число осмотров",
    avgInProccess: "Среднее время на взятие в работу",
    avgDecision: "Среднее время на принятие решения",
};

function getQuery(params) {

    const { startDate, finishDate } = params;

    const query = {
        values: [startDate, finishDate],
        text: `SELECT TO_CHAR($1::timestamptz, 'YYYY-MM-DD')||' - '||TO_CHAR($2::timestamptz, 'YYYY-MM-DD') as "${headers.period}",
                    dm.id as "${headers.id}",
                    dm.surname||' '||dm.name||' '||dm.patronymic as "${headers.medFio}",
                    COUNT(dmr."medData_id") as "${headers.viewsCount}",
                    round(AVG(l1.diff)::numeric, 2) as "${headers.avgInProccess}",
                    round(AVG(l2.diff)::numeric, 2) as "${headers.avgDecision}"
                FROM drivers_medics dm
                   JOIN drivers_medresult dmr ON dm.id=dmr."medUser_id"   
                   JOIN drivers_meddata dmd ON dmd.id=dmr."medData_id"
                
                   JOIN(SELECT AVG(EXTRACT(EPOCH FROM dd2.date - dd1.date)) as diff, dd1."medData_id"
                    FROM drivers_meddatalog dd1
                    JOIN drivers_meddatalog dd2 ON dd2."medData_id"=dd1."medData_id"
                    WHERE dd1."ActionState"=2 AND dd2."ActionState"=3 GROUP BY dd1."medData_id") l1 ON l1."medData_id"=dmr."medData_id"
                
                   JOIN(SELECT EXTRACT(EPOCH FROM max(dd2.date - dd1.date)) as diff, dd1."medData_id"
                    FROM drivers_meddatalog dd1
                    JOIN drivers_meddatalog dd2 ON dd2."medData_id"=dd1."medData_id"
                    WHERE dd1."ActionState"=4 AND dd2."ActionState"=5 GROUP BY dd1."medData_id") l2 ON l2."medData_id"=dmr."medData_id"
                
                 WHERE dmd.date BETWEEN $1::timestamptz AND $2::timestamptz AND dm.group_medic IN(1,2)
                
                 GROUP BY dm.id
                 ORDER BY "${headers.medFio}";`
    };

    return query;
}

function getHeaders() {
    return [
        headers.period,
        headers.id,
        headers.medFio,
        headers.viewsCount,
        headers.avgInProccess,
        headers.avgDecision,
    ];
}

module.exports = {
    getQuery,
    getHeaders
}
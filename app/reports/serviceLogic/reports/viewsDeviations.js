const { concatDateAndTime } = require('../../../utils/dbHelp');

const headers = {
    id: "id",
    medFio: "ФИО медработника",
    dmrId: "id осмотра",
    comment: "Отклонение по показателю",
}

function getQuery(params) {

    const { startDate, startTime, finishDate, finishTime } = params;

    const start = concatDateAndTime(startDate, startTime);
    const finish = concatDateAndTime(finishDate, finishTime);

    const query = {
        values: [start, finish],
        text: `SELECT dm.id as "${headers.id}",
                    dm.surname||' '||dm.name||' '||dm.patronymic as "${headers.medFio}",
                    dmr.id as "${headers.dmrId}",

                     CONCAT_WS(', ',
                     
                                CASE WHEN (dc.driver_id IS NOT NULL AND (dmd."pressureUpper" NOT BETWEEN dc."pressureUpperMin" AND dc."pressureUpperMax" OR
                                             dmd."pressureLower" NOT BETWEEN dc."pressureLowerMin" AND dc."pressureLowerMax")
                             ) OR 
                             (ml.ord_id IS NOT NULL AND (dmd."pressureUpper" NOT BETWEEN ml."pressureUpperMin" AND ml."pressureUpperMax" OR
                                             dmd."pressureLower" NOT BETWEEN ml."pressureLowerMin" AND ml."pressureLowerMax")
                             )OR
                             (ml.ord_id IS NULL AND dc.driver_id IS NULL AND (dmd."pressureUpper" NOT BETWEEN 100 AND 150 OR dmd."pressureLower" NOT BETWEEN 60 AND 100))
                                 THEN 'АД'
                         ELSE NULL 
                         END,
                    
                         CASE WHEN (dc.driver_id IS NOT NULL AND dmd.pulse NOT BETWEEN dc."pulseMin" AND dc."pulseMax")
                             OR 
                             (ml.ord_id IS NOT NULL AND dmd.pulse NOT BETWEEN ml."pulseMin" AND ml."pulseMax")
                             OR
                             (ml.ord_id IS NULL AND dc.driver_id IS NULL AND dmd.pulse NOT BETWEEN 54 AND 96)
                                 THEN 'ЧСС'
                         ELSE NULL 
                         END,
                    
                         CASE WHEN cast (dmd.alcho as numeric) > 0.0 THEN 'АЛКО'
                         ELSE NULL 
                         END,
                    
                         CASE WHEN dmd.temperature <>'' AND (
                             (dc.driver_id IS NOT NULL AND cast (dmd.temperature as numeric) NOT BETWEEN 35.5 AND 37.0)
                             OR
                             (ml.ord_id IS NOT NULL AND cast (dmd.temperature as numeric) NOT BETWEEN 35.5 AND ml."temperatureMax")
                             OR
                             (ml.ord_id IS NULL AND dc.driver_id IS NULL AND cast (dmd.temperature as numeric) NOT BETWEEN 35.5 AND 37.0)
                             )
                                 THEN 'Температура'
                         ELSE NULL 
                         END
                            ) as "${headers.comment}"
                        
                     FROM drivers_meddata dmd
                               JOIN drivers_medresult dmr ON dmr."medData_id"=dmd.id
                             JOIN drivers_medics dm ON dm.id=dmr."medUser_id"
                                       JOIN drivers_driver dd ON dmd.driver_id=dd.id
                                        LEFT JOIN drivers_conf dc ON dmd.driver_id=dc.driver_id
                                       LEFT JOIN medical_limits ml ON ml.ord_id=dd.org_id
                                 WHERE dmr."medResult"=TRUE
                                     AND (dmd.date BETWEEN $1::timestamptz AND $2::timestamptz)
                                     AND dmd.beforeafter IN('before', 'line') AND dm.group_medic IN (1,2)
                                     AND ml.date_end IS NULL OR ml.date_end > now();`
    };

    return query;
}

function postProcessingHandler(rawData) {

    return rawData.filter(raw => raw[headers.comment] !== '');
}

function getHeaders() {
    return [
        headers.id,
        headers.medFio,
        headers.dmrId,
        headers.comment,
    ];
}

module.exports = {
    getQuery,
    getHeaders,
    postProcessingHandler
}
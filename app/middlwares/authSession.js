
const jwt = require('jsonwebtoken');

const constants = require('../utils/constants');

async function isUserAuth(req, res, next) {
    try {
        const decoded = await _verifyToken(req.body.token)

        req.body.client_id = decoded.client_id;
        req.body.role_id = decoded.role_id;

        if (req.body.role_id < constants.AUTH_ROLES.USER)
            return res.status(403).send();

        return next();
    }
    catch (err) {
        console.log(err);
        res.status(500).send();
    }
}

async function isAdminAuth(req, res, next) {
    try {
        const decoded = await _verifyToken(req.body.token)

        req.body.client_id = decoded.client_id;
        req.body.role_id = decoded.role_id;

        if (req.body.role_id < constants.AUTH_ROLES.ADMIN)
            return res.status(403).send();

        return next();
    }
    catch (err) {
        console.log(err);
        res.status(500).send();
    }
}

function _verifyToken(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.TOKEN_PRIVATE_KEY, (err, decoded) => {
            if (err) {
                return reject(err);
            }
            resolve(decoded);
        });
    });
}

function createToken(tokenData) {
    return new Promise((resolve, reject) => {
        jwt.sign(tokenData, process.env.TOKEN_PRIVATE_KEY, function (err, token) {

            if (err) {
                return reject(err);
            }
            resolve(token);
        });
    });
}

module.exports = {
    isUserAuth,
    isAdminAuth,
    createToken
}



const moment = require('moment');
const crypto = require('crypto');

function concatDateAndTime(date, time) {
    return `${date}T${time}`;
}

function setTimeZone(date) {
    if (date.includes('T'))
        return moment(date).utcOffset('+0300').toISOString();

    return moment(date).utcOffset('+0300').toISOString();
}

function comparePassword(params) {
    const { password, salt, hashedPassword } = params;
    
    const hmac = crypto.createHmac('sha256', salt);

    hmac.update(password);

    return hmac.digest('hex') === hashedPassword;
}

function generateSaltAndHashedPassword(password) {
    const salt = crypto.randomBytes(256).toString('hex').substr(20, 40);

    const hmac = crypto.createHmac('sha256', salt);

    hmac.update(password);

    const hashedPassword = hmac.digest('hex');

    return {
        salt,
        hashedPassword
    };
}

module.exports = {
    concatDateAndTime,
    setTimeZone,
    comparePassword,
    generateSaltAndHashedPassword
}
module.exports = (() => {
    const AUTH_ROLES = {
        USER: 1,
        ADMIN: 2
    };

    const WORK_STATUS = {
        FREE: 1,
        BUSY: 2,
        DONE: 3,
        FAIL: 4
    };

    const REGIONS = {
        GENERAL: 'general',
        TVER: 'tver',
        MOSCOW: 'moscow'
    };

    const MAILING_TYPES = {
        EMAIL: 'email',
        SMS: 'sms'
    };

    const VIEW_TYPES = {
        before: 'before',
        after: 'after',
        beforeshift: 'beforeshift',
        aftershift: 'aftershift',
        line: 'line',
        alcho: 'alcho'
    };

    const VIEW_TYPE_MAPPING = {
        [VIEW_TYPES.before]: 'Предрейсовый',
        [VIEW_TYPES.after]: 'Послерейсовый',
        [VIEW_TYPES.beforeshift]: 'Предсменный',
        [VIEW_TYPES.aftershift]: 'Послесменный',
        [VIEW_TYPES.line]: 'Линейный',
    }

    return {
        AUTH_ROLES,
        WORK_STATUS,
        REGIONS,
        MAILING_TYPES,
        VIEW_TYPES,
        VIEW_TYPE_MAPPING
    };
})();
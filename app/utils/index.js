const iconv = require('iconv-lite');
const fs = require('fs');
const util = require('util');

const promisifyWriteFile = util.promisify(fs.writeFile);

const config = require('../../config');

function validateIntervalDate(startDate, finishDate) {

    const validateStartDate = new Date(startDate);
    const validateFinishDate = new Date(finishDate);

    if (validateStartDate > validateFinishDate)
        throw new MyError('Неверный интервал дат');
}

function validateTime(time) {

    if (!time.match(/^\d{2}:\d{2}$/))
        throw new MyError('Неверный формат времени');

    const [hours, minutes] = time.split(':');


    if ((hours > 23 && hours < 0) || (minutes > 59 && minutes < 0))
        throw new MyError('Неверное время');
}

function validateIntervalDatetime(startDate, startTime, finishDate, finishTime) {
    const start = new Date(`${startDate}T${startTime}`);
    const finish = new Date(`${finishDate}T${finishTime}`);

    if (start > finish)
        throw new MyError('Неверный интервал дат');
}

function validateDate(date) {

    if (!date.match(/^\d{4}-\d{2}-\d{2}$/))
        throw new MyError('Неверный формат даты');

    const day = date.split('-')[2];

    const validateDate = new Date(date);

    const isValidateDate = (Boolean(+validateDate) && validateDate.getDate() == day);

    if (!isValidateDate)
        throw new MyError('Неверная дата');
}


function createCSVFile(headers, rawData, path, name, id) {
    const fileData = getCSVFileData(headers, rawData);

    return _saveFile(path, name, id, fileData);
}

function getCSVFileData(headers, rawRows) {

    const csv = [];

    csv.push(headers.join('\t'));

    if (rawRows.length !== 0) {
        rawRows.forEach(rawRow =>
            csv.push(headers.map(header => rawRow[header]).join('\t'))
        );
    }

    return iconv.encode(csv.join('\r\n'), 'utf16');
};

async function _saveFile(path, name, id, data) {

    const pathFile = `${path}/${name}_${id}.csv`;

    await promisifyWriteFile(pathFile, data);

    return pathFile;
}


class MyError {
    constructor(message) {
        this.message = message;
    }

    static create(message) {
        return new MyError(message);
    }
}

module.exports = {
    validateDate,
    validateTime,
    validateIntervalDate,
    validateIntervalDatetime,
    createCSVFile,
    MyError,
    getCSVFileData
}
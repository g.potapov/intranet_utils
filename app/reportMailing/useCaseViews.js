const moment = require('moment');

const IReport = require('./ReportEntity');
const utils = require('../utils');
const constants = require('../utils/constants.js');

const reportAccessLogic = require('../reports/serviceLogic/reportAccessLogic');
const caller = require('../utils/caller');

class UseCaseViews extends IReport {

    constructor(params) {
        super(params);

        this.recipients = params.recipients;
        this.mailingType = params.mailingType;

        this.headers = {
            period: "Период",
            orgId: "id организации",
            orgName: "Наименование организации",
            parentOrgName: "Родительская организация",
            countViews: "Количество осмотров"
        };

        this.currentDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    }

    static create(params) {
        return new UseCaseViews(params);
    }

    execReport(sqlQuery) {
        return reportAccessLogic.execReport(sqlQuery);
    }

    getReportSql() {
        const query = {
            values: [this.currentDate],
            text: `SELECT $1 as "${this.headers.period}",
                        d_org_1.id as "${this.headers.orgId}",
                        d_org_1.name as "${this.headers.orgName}",
                        d_org_2.name as "${this.headers.parentOrgName}",
                        COUNT(*) as "${this.headers.countViews}"
                    FROM drivers_meddata dmd
                        JOIN drivers_driver dd ON dd.id=dmd.driver_id
                        JOIN drivers_organization d_org_1 ON d_org_1.id=dd.org_id
                        LEFT JOIN drivers_organization d_org_2 ON d_org_2.id=d_org_1.link_id
                    WHERE concat_ws(' ', dd.surname, dd.name, dd.patronymic) NOT LIKE '%test%'
                         AND dd.number NOT IN ('0000', '000000') 
                         AND TO_CHAR(dmd.date AT TIME ZONE 'gmt-3', 'YYYY-MM-DD') = $1
                    GROUP BY "${this.headers.period}", "${this.headers.orgId}", "${this.headers.orgName}", "${this.headers.parentOrgName}";`
        }

        return query;
    }

    postExecHandler(sqlData) {
        return utils.getCSVFileData(this.getHeaders(), sqlData);
    }

    getHeaders() {
        return [
            this.headers.period,
            this.headers.orgId,
            this.headers.orgName,
            this.headers.parentOrgName,
            this.headers.countViews
        ];
    }

    async sendReport({ html, csvData }) {
        const mailingData = {
            channel: this.mailingType,
            responders: this.recipients,
            body: {
                stampId: "emptyHtml",
                subject: `Отчёт по осмотрам за ${this.currentDate}`,
                html
            },
            attachments: [
                {
                    filename: `отчет_по_осмотрам_${this.currentDate}.csv`,
                    content: csvData
                }
            ]
        }

        try {
            await caller({
                method: 'POST',
                uri: process.env.MAILING_SERVICE_URL,
                body: mailingData
            });
        }
        catch (err) {
            console.log(err);
            throw new Error(`error througth calling mailing service, err - ${err.message}`);
        }
    }

    async process() {

        const sqlData = await this.execReport(this.getReportSql());

        const csvData = this.postExecHandler(sqlData);

        await this.sendReport({
            html: {},
            csvData: Buffer.from(csvData).toJSON().data
        });
    }
}

module.exports = async () => {

    const recipients = [
        /* 'a.vanin@medpoint24.ru',
         'krajnik@medpoint24.ru',
         'a.kiselev@medpoint.ru',
         'e.kiseleva@medpoint.ru',
         'o.savinov@medpoint.ru',
         'a.rychkova@medpoint24.ru'*/
        'g.potapov@medpoint24.ru'
    ]

    const instance = UseCaseViews.create({
        recipients,
        mailingType: constants.MAILING_TYPES.EMAIL
    });

    try {
        await instance.process();
    }
    catch (err) {
        console.log(`Report by views not send, reason: ${err.message}, stack - `, err.stack);
    }
}
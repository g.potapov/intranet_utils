const reportAccessLogic = require('../reports/serviceLogic/reportAccessLogic');
const caller = require('../utils/caller');
const constants = require('../utils/constants');
const config = require('../../config');

const moment = require('moment');

const IReport = require('./ReportEntity');

class GroupMailing extends IReport {
    constructor(params) {
        super(params);
        this.orgId = params.orgId;
        this.orgName = params.orgName;
        this.recipients = params.recipients;
        this.mailingType = params.mailingType;
        this.currentDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    }

    static create(params) {
        return new GroupMailing(params);
    }

    getReportSql() {

        const query = {
            values: [this.orgId, this.currentDate],
            text: `SELECT dd.id as id, array_agg(dmr."medResult"||';'||dmd.beforeafter||';'||dmr.comment) as arr
                        FROM drivers_medresult dmr
                            JOIN drivers_meddata dmd ON dmr."medData_id"=dmd.id
                            JOIN drivers_driver dd ON dd.id=dmd.driver_id
                            JOIN drivers_organization d_org ON d_org.id=dd.org_id
                    WHERE d_org.id = $1 AND dmd.beforeafter IN('before', 'after', 'beforeshift', 'aftershift', 'line') 
                            AND TO_CHAR(dmd.date AT TIME ZONE 'gmt-3', 'YYYY-MM-DD') = $2
                    GROUP BY dd.id;`
        }

        return query;
    }

    postExecHandler(sqlData) {

        const viewTypes = {
            before: "before",
            after: "after",
            line: "line",
            beforeshift: "beforeshift",
            aftershift: "aftershift",
            alcho: "alcho"
        }

        const postExecData = {

            total: 0,
            total_accepted: 0,
            total_rejected: 0,

            before: {
                total: 0,
                accepted: 0,
                rejected: 0,
                rejected_health: 0,
                rejected_alcho: 0,
                rejected_other: 0,
            },

            beforeshift: {
                total: 0,
                accepted: 0,
                rejected: 0,
                rejected_health: 0,
                rejected_alcho: 0,
                rejected_other: 0,
            },

            line: {
                total: 0,
                accepted: 0,
                rejected: 0,
                rejected_health: 0,
                rejected_alcho: 0,
                rejected_other: 0,
            },

            after: {
                total: 0,
            },

            aftershift: {
                total: 0,
            },

            orgName: this.orgName,
            orgId: this.orgId
        }

        postExecData.total = sqlData.length;


        const groupByDriverIdAndViewType = sqlData.map(({ arr }) => {

            const tmp = {};

            arr.forEach(elem => {

                const [medResult, viewType, comment] = elem.split(';');

                if (!tmp[viewType])
                    tmp[viewType] = [];

                tmp[viewType].push({ medResult, comment });
            })

            return tmp;
        });

        groupByDriverIdAndViewType.forEach(item => {

            Object.entries(item).forEach(([viewType, arr]) => {

                if (!arr.length)
                    return;

                postExecData[viewType].total++;

                if ([viewTypes.before, viewTypes.beforeshift, viewTypes.line].some(type => type === viewType)) {

                    //TODO fix comparation
                    if (arr.every(({ medResult }) => medResult === 'true')) {
                        postExecData[viewType].accepted++;
                    }
                    else {
                        postExecData[viewType].rejected++;

                        const { comment } = arr.pop();

                        const byAlcho = /паров/.test(comment);
                        const byOther = /вне зоны|убор\/очки\/верхнюю|Сбой показателей|Нарушение|не загружается/.test(comment);
                        const byHealth = /АД|ЧСС|покровов|жалоб/.test(comment);

                        if ((byHealth && byAlcho && byOther) || byHealth) {
                            postExecData[viewType].rejected_health++;
                        }
                        else if (byAlcho) {
                            postExecData[viewType].rejected_alcho++;
                        }
                        else if (byOther) {
                            postExecData[viewType].rejected_other++;
                        }
                    }
                }
            });
        });

        return postExecData;
    }

    execReport(sqlQuery) {
        return reportAccessLogic.execReport(sqlQuery);
    }

    async getJournalFile() {

        const body = {
            user: config.testUserId,
            config: config.testUserConfig,
            id: `${this.orgId}`,
            date: `${this.currentDate}`,
            name: `${this.orgName}`,
            verboseDay: true
        }

        try {
            return caller({
                method: 'POST',
                uri: process.env.FILEGEN_MEDIATOR_URL,
                body
            });
        }
        catch (err) {
            console.log(err);
            throw new Error(`error througth calling filegen mediator service, err - ${err.message}`);
        }

    }

    async sendReport({ html, journalData }) {

        const mailingData = {
            channel: this.mailingType,
            responders: this.recipients,
            body: {
                stampId: "dymov",
                subject: `Отчёт по медосмотрам за ${this.currentDate}`,
                html
            },
            attachments: [
                {
                    filename: `журнал_${this.currentDate}.xlsx`,
                    content: journalData
                }
            ]
        }

        try {
            await caller({
                method: 'POST',
                uri: process.env.MAILING_SERVICE_URL,
                body: mailingData
            });
        }
        catch (err) {
            console.log(err);
            throw new Error(`error througth calling mailing service, err - ${err.message}`);
        }
    }

    async process() {
        const sqlData = await this.execReport(this.getReportSql());

        const html = this.postExecHandler(sqlData);
        const journalData = await this.getJournalFile();

        await this.sendReport({
            html,
            journalData: Buffer.from(journalData).toJSON().data
        });
    }
}

module.exports = async () => {

    const ORG_ID_MAPPING = {
        DYMOV: {
            id: 328,
            name: 'ООО Дымовское колбасное производство'
        },
        /*
                LIKID: {
                    id: 489,
                    name: 'ООО ЭР ЛИКИД'
                },
        
                TRASTLINE: {
                    id: 271,
                    name: 'ООО ТРАСТЛАЙН'
                },
                SBERBANK: {
                    id: 211,
                    name: 'ПАО Сбербанк'
                },
                NOKIA: {
                    id: 401,
                    name: 'АО «Нокиа Солюшнз энд Нетворкс»'
                }*/
    }

    const RECIPIENTS_MAPPING = {
        [ORG_ID_MAPPING.DYMOV.id]: [
            /*'Sergeeva.Svetlana@v-dymov.ru',
            'Darbinyan.Burastan@v-dymov.ru',
            'a.kiselev@medpoint24.ru'*/
            'g.potapov@medpoint24.ru'
        ],
        /*
        [ORG_ID_MAPPING.LIKID.id]: [
            'aleksey.skachkov@airliquide.com',
            'o.savinov@medpoint24.ru'
        ],

        [ORG_ID_MAPPING.TRASTLINE.id]: [
            'petrovan@iek.ru',
            'o.savinov@medpoint24.ru'
        ],

        [ORG_ID_MAPPING.SBERBANK.id]: [
            'VVTitov@sberbank.ru',
            'o.savinov@medpoint24.ru'
        ],

        [ORG_ID_MAPPING.NOKIA.id]: [
            'dmitry.sidorovsky@nokia.com',
            'o.savinov@medpoint24.ru'
        ]*/
    }

    const processTasks = Object.values(ORG_ID_MAPPING).map(
        async ({ id, name }) => {
            try {
                const instance = GroupMailing.create({
                    orgId: id,
                    orgName: name,
                    mailingType: constants.MAILING_TYPES.EMAIL,
                    recipients: RECIPIENTS_MAPPING[id]
                });

                await instance.process();

                return '';
            }
            catch (err) {
                console.log(`error stack - ${err.stack}`);
                return `Report with orgId ${id} not send, reason: ${err.message}`;
            }
        }
    );

    await Promise.all(processTasks)
        .then(result => {
            if (result.every(res => res === '')) {
                console.log('all reports success sended to mailing service');
            }
            else {
                console.log(result.join(', '));
            }
        });
}




const { queryToReportsPostgres,
    queryToMedistPostgres,
    selectOneFromReports } = require('./dbConnectPostgres');
const config = require('../../config');
const constants = require('../utils/constants');


function saveReportRequestToBd(reportParams, reportName, clientId) {

    const query = {
        values: [reportParams, reportName, clientId],
        text: 'INSERT INTO reports_status(query, name, client_id) VALUES ($1, $2, $3)'
    };

    return queryToReportsPostgres(query);
}

function getReportsRowsFromBd() {

    const query = {
        values: [constants.WORK_STATUS.BUSY, config.reportsSize],
        text: `UPDATE reports_status SET status=$1 WHERE id IN (SELECT id FROM reports_status WHERE status=1 LIMIT($2)) RETURNING *`
    }

    return queryToReportsPostgres(query);
}

function executeReportsQuery(reportQuery) {
    return queryToMedistPostgres(reportQuery);
}

function updateReportStatus(id, status) {

    if (status === constants.WORK_STATUS.DONE) {
        return queryToReportsPostgres(`UPDATE reports_status SET status=${status}, creation_date=NOW() WHERE id=${id}`);
    }
    else {
        return queryToReportsPostgres(`UPDATE reports_status SET status=${status} WHERE id=${id}`);
    }
}

function getSaltByLogin(login) {

    const saltQuery = {
        values: [login],
        text: 'SELECT salt FROM users WHERE login=$1'
    }

    return selectOneFromReports(saltQuery);
}

function getClientData(login) {

    const authQuery = {
        values: [login],
        text: 'SELECT id as client_id, salt, hashed_pass, role_id FROM users WHERE login=$1'
    }

    return selectOneFromReports(authQuery);
}

function checkIfClientExistByLogin(login) {

    const authQuery = {
        values: [login],
        text: 'SELECT * FROM users WHERE login=$1'
    }

    return selectOneFromReports(authQuery);
}

function saveUserInDb(params) {

    const { login, hashedPassword, salt, new_role_id } = params;

    const authQuery = {
        values: [new_role_id, login, hashedPassword, salt],
        text: 'INSERT INTO users(role_id, login, hashed_pass, salt) VALUES ($1, $2, $3, $4)'
    }

    return queryToReportsPostgres(authQuery);
}

function getReportStatusesFromBd(clientId, reportName) {
    const query = {
        values: [clientId, reportName, config.viewSize],
        text: `SELECT id, TO_CHAR(creation_date AT TIME ZONE 'gmt-3', 'YYYY-MM-DD HH24:MI:SS') as creation_date, status FROM reports_status WHERE client_id=$1 AND name=$2 ORDER BY id DESC LIMIT($3)`
    }

    return queryToReportsPostgres(query);
}

function getOrganizationsFromDb() {

    return queryToMedistPostgres(`SELECT id, name FROM drivers_organization`);
}

module.exports = {
    saveReportRequestToBd,
    getReportsRowsFromBd,
    executeReportsQuery,
    updateReportStatus,
    getSaltByLogin,
    getClientData,
    saveUserInDb,
    checkIfClientExistByLogin,
    getReportStatusesFromBd,
    getOrganizationsFromDb
}
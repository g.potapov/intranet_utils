const { Pool } = require('pg');

const MedistPool = new Pool({
    connectionString: process.env.ONLY_READ_MEDIST_DB
});

const ReportsPool = new Pool({
    connectionString: process.env.REPORTS_DB
});

MedistPool.on('error', (err, client) => {
    console.log('Unexpected error on idle client', err);
});

ReportsPool.on('error', (err, client) => {
    console.log('Unexpected error on idle client', err);
});

function queryToMedistPostgres(query) {

    return MedistPool.query(query)
        .then(result => result.rows)
        .catch(err => console.log(`bad sql, err - ${err.message}, text - ${query.text}, params - ${query.values}`));
}

function queryToReportsPostgres(query) {

    return ReportsPool.query(query)
        .then(result => result.rows)
        .catch(err => console.log(`bad sql, err - ${err.message}, text - ${query.text}, params - ${query.values}`));
}

function selectOneFromReports(query) {
    return queryToReportsPostgres(query)
        .then(rows => rows[0]);
}

module.exports = {
    queryToMedistPostgres,
    queryToReportsPostgres,
    selectOneFromReports,
    ReportsPool
}


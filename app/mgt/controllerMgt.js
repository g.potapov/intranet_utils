const medistModel = require('../dao/daoPg');
const useCaseMgtMapping = require('./useCaseMapping');

async function getMgtStatistics(req, res, next) {

    try {
        const { mgtType, ...request } = req.body;

        const data = await useCaseMgtMapping(mgtType)(request);

        res.status(200).send(data);
    }
    catch (err) {
        res.status(err.status).send({
            message: err.message
        });
    }
};

async function getOrganizations(req, res, next) {
    try {
        const organizations = await medistModel.getOrganizationsFromDb();

        res.status(200).send(organizations);
    }
    catch (err) {
        res.status(500).send({
            message: 'error througth get organizations list'
        });
    }
}

module.exports = {
    getMgtStatistics,
    getOrganizations
}
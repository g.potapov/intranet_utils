const byViews = require('./useCaseMgtViews');
const byViewTime = require('./useCaseMgtViewTime');
const createError = require('http-errors');

const MAPPING = {
    views: byViews,
    viewTime: byViewTime
}

module.exports = (mgtType) => {
    if (MAPPING[mgtType])
        return MAPPING[mgtType];

    throw createError(400, `type ${mgtType} doesnt exist`);
}
const createError = require('http-errors');
const moment = require('moment');

const IReport = require('../reportMailing/ReportEntity');

const constants = require('../utils/constants');
const medistModel = require('../dao/daoPg');
const helpers = require('../utils');
const { ValidationError } = require('../utils/errors');
const utils = require('../utils');
const config = require('../../config');

class MgtByViews extends IReport {

    constructor(params) {
        super();

        this.csvHeaders = {
            date: 'Дата',
            viewType: 'Тип осмотра',
            orgName: 'Организация',
            hostname: 'Точка осмотра',
            viewResult: 'Результат осмотра',
            count: 'Количество'
        }

        this.sqlHeaders = {
            dt: 'dt',

            hostname: 'hostname',
            orgname: 'orgname',

            total: 'total',
            totalAccepted: 'totalAccepted',
            totalRejected: 'totalRejected',

            beforeAccepted: 'beforeAccepted',
            beforeRejected: 'beforeRejected',

            afterAccepted: 'afterAccepted',
            afterRejected: 'afterRejected',

            beforeshiftAccepted: 'beforeshiftAccepted',
            beforeshiftRejected: 'beforeshiftRejected',

            aftershiftAccepted: 'aftershiftAccepted',
            aftershiftRejected: 'aftershiftRejected',

            lineAccepted: 'lineAccepted',
            lineRejected: 'lineRejected'
        }

        this.RESULT_TYPES = {
            ACCEPTED: 'accepted',
            REJECTED: 'rejected'
        }

        this.viewTypes = params.viewTypes;
        this.resultTypes = params.resultTypes;
        this.orgIds = params.orgIds;
        this.beginDate = params.beginDate;
        this.endDate = params.endDate;
        this.hostnames;
    }

    static create(params) {
        return new MgtByViews(params);
    }

    getBeginDate() {
        return this.beginDate;
    }

    getEndDate() {
        return this.endDate;
    }

    _formatSqlByViewType() {
        const subStr = [];

        this.viewTypes.forEach(viewType => {
            switch (viewType) {
                case constants.VIEW_TYPES.before: {
                    if (this.resultTypes.includes(this.RESULT_TYPES.ACCEPTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='before' AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.beforeAccepted}"`);
                    if (this.resultTypes.includes(this.RESULT_TYPES.REJECTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='before' AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.beforeRejected}"`);
                }
                    break;

                case constants.VIEW_TYPES.after: {
                    if (this.resultTypes.includes(this.RESULT_TYPES.ACCEPTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='after' AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.afterAccepted}"`);
                    if (this.resultTypes.includes(this.RESULT_TYPES.REJECTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='after' AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.afterRejected}"`);
                }
                    break;

                case constants.VIEW_TYPES.line: {
                    if (this.resultTypes.includes(this.RESULT_TYPES.ACCEPTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='line' AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.lineAccepted}"`);
                    if (this.resultTypes.includes(this.RESULT_TYPES.REJECTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='line' AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.lineRejected}"`);
                }
                    break;

                case constants.VIEW_TYPES.beforeshift: {
                    if (this.resultTypes.includes(this.RESULT_TYPES.ACCEPTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='beforeshift' AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.beforeshiftAccepted}"`);
                    if (this.resultTypes.includes(this.RESULT_TYPES.REJECTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='beforeshift' AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.beforeshiftRejected}"`);
                }
                    break;

                case constants.VIEW_TYPES.aftershift: {
                    if (this.resultTypes.includes(this.RESULT_TYPES.ACCEPTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='aftershift' AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.aftershiftAccepted}"`);
                    if (this.resultTypes.includes(this.RESULT_TYPES.REJECTED))
                        subStr.push(`COUNT(CASE WHEN dmd.beforeafter='aftershift' AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.aftershiftRejected}"`);
                }
                    break;
            }
        });

        return subStr;
    }

    getReportSql() {

        const subStr = this._formatSqlByViewType(this.viewTypes);

        const query = {
            values: [this.beginDate, this.endDate, this.orgIds, this.hostnames],
            text: `SELECT to_date(TO_CHAR(dmr.date, 'DD MM YYYY'), 'DD.MM.YYYY') as "${this.sqlHeaders.dt}",
                        dmd.hostname as "${this.sqlHeaders.hostname}",
                        d_org.name as "${this.sqlHeaders.orgname}",
                        ${subStr.join(',\n')}
                    FROM drivers_meddata dmd
                    	JOIN drivers_medresult dmr ON dmd.id=dmr."medData_id"
                    	JOIN drivers_driver dd ON dmd.driver_id = dd.id
                    	JOIN drivers_organization d_org ON d_org.id=dd.org_id
                    WHERE d_org.id = ANY($3) AND dmr.date BETWEEN $1::timestamptz AND $2::timestamptz AND dmd.hostname = ANY($4)
                    GROUP BY "${this.sqlHeaders.dt}", "${this.sqlHeaders.orgname}", "${this.sqlHeaders.hostname}" 
                    ORDER BY "${this.sqlHeaders.dt}";`
        }

        return query;
    }

    getReportSqlTotalByOrg() {

        const subStr = this._formatSqlByViewType(this.viewTypes);

        const query = {
            values: [this.beginDate, this.endDate, this.orgIds, this.viewTypes],
            text: `SELECT to_date(TO_CHAR(dmr.date, 'DD MM YYYY'), 'DD.MM.YYYY') as "${this.sqlHeaders.dt}",
                            COUNT(CASE WHEN dmd.beforeafter = ANY($4) THEN 1 END) as "${this.sqlHeaders.total}",
                            COUNT(CASE WHEN dmd.beforeafter = ANY($4) AND dmr."medResult"=true THEN 1 END) as "${this.sqlHeaders.totalAccepted}",
                            COUNT(CASE WHEN dmd.beforeafter = ANY($4) AND dmr."medResult"=false THEN 1 END) as "${this.sqlHeaders.totalRejected}",
                        ${subStr.join(',\n')}
                    FROM drivers_meddata dmd
                    	JOIN drivers_medresult dmr ON dmd.id=dmr."medData_id"
                    	JOIN drivers_driver dd ON dmd.driver_id = dd.id
                    	JOIN drivers_organization d_org ON d_org.id=dd.org_id
                    WHERE d_org.id = ANY($3) AND dmr.date BETWEEN $1::timestamptz AND $2::timestamptz
                    GROUP BY "${this.sqlHeaders.dt}" 
                    ORDER BY "${this.sqlHeaders.dt}";`
        }

        return query;
    }

    async setHostnames() {
        const query = {
            values: [this.orgIds],
            text: `SELECT DISTINCT dmd.hostname                     
                        FROM drivers_meddata dmd
                            JOIN drivers_medresult dmr ON dmd.id=dmr."medData_id"
                            JOIN drivers_driver dd ON dmd.driver_id = dd.id
                            JOIN drivers_organization d_org ON d_org.id=dd.org_id
                    WHERE d_org.id = ANY($1) AND dmd.date BETWEEN NOW() - INTERVAL '3 month' AND NOW();`
        }

        const hostnames = await this.execReport(query).then(res => res.map(({ hostname }) => hostname));

        this.hostnames = hostnames;
    }

    _getViewTypeAndResultBySqlHeader(sqlHeader) {

        const result = {
            viewResult: null,
            viewType: null
        }

        if (/^before/.test(sqlHeader))
            result.viewType = constants.VIEW_TYPE_MAPPING[constants.VIEW_TYPES.before];
        else if (/^beforeshift/.test(sqlHeader))
            result.viewType = constants.VIEW_TYPE_MAPPING[constants.VIEW_TYPES.beforeshift];
        else if (/^after/.test(sqlHeader))
            result.viewType = constants.VIEW_TYPE_MAPPING[constants.VIEW_TYPES.after];
        else if (/^aftershift/.test(sqlHeader))
            result.viewType = constants.VIEW_TYPE_MAPPING[constants.VIEW_TYPES.aftershift];
        else if (/^line/.test(sqlHeader))
            result.viewType = constants.VIEW_TYPE_MAPPING[constants.VIEW_TYPES.line];

        switch (sqlHeader) {
            case this.sqlHeaders.beforeAccepted:
            case this.sqlHeaders.afterAccepted:
            case this.sqlHeaders.lineAccepted:
                result.viewResult = 'Допущен';
                break;

            case this.sqlHeaders.beforeRejected:
            case this.sqlHeaders.afterRejected:
            case this.sqlHeaders.lineRejected:
                result.viewResult = 'Не допущен';
                break;

            case this.sqlHeaders.beforeshiftRejected:
            case this.sqlHeaders.aftershiftRejected:
                result.viewResult = 'Не прошел';
                break;

            case this.sqlHeaders.beforeshiftAccepted:
            case this.sqlHeaders.aftershiftAccepted:
                result.viewResult = 'Валидирован';
                break;
        }

        return result;
    }

    getHeaders() {
        return [
            this.csvHeaders.date,
            this.csvHeaders.viewType,
            this.csvHeaders.orgName,
            this.csvHeaders.hostname,
            this.csvHeaders.viewResult,
            this.csvHeaders.count
        ]
    }

    validate() {

        const resultValidation = [];

        if (!this.orgIds)
            resultValidation.push('missing orgIds parameter');

        if (!this.viewTypes)
            resultValidation.push('missing viewTypes parameter');

        if (!this.resultTypes)
            resultValidation.push('missing resultTypes parameter');

        if (!this.beginDate)
            resultValidation.push('missing beginDate parameter');

        if (!this.endDate)
            resultValidation.push('missing endDate parameter');

        if (resultValidation.length)
            throw new ValidationError(resultValidation.join(', '));

        if (!Array.isArray(this.orgIds))
            resultValidation.push('orgIds type is incorrect. Need array');
        else if (this.orgIds.some(orgId => !Number.isInteger(orgId)))
            resultValidation.push('one or more orgId type is incorrect. Need integer');

        if (!Array.isArray(this.viewTypes))
            resultValidation.push('viewTypes type is incorrect. Need array');
        else {
            const arr = Object.values(constants.VIEW_TYPES);

            if (this.viewTypes.some(viewType => arr.includes(viewType) === false))
                resultValidation.push('one or more viewTypes is unknown');
        }

        if (!Array.isArray(this.resultTypes))
            resultValidation.push('resultTypes type is incorrect. Need array');
        else {
            const arr = Object.values(this.RESULT_TYPES);

            if (this.resultTypes.some(resultType => arr.includes(resultType) == false))
                resultValidation.push('one or more resultTypes is unknown');
        }

        // непродуманный заранее код - говнокод (самобичевание наше все) 
        try {
            utils.validateDate(this.beginDate);
        }
        catch (err) {
            resultValidation.push('beginDate incorrect');
        }

        try {
            utils.validateDate(this.endDate);
        }
        catch (err) {
            resultValidation.push('endDate incorrect');
        }

        try {
            utils.validateIntervalDate(this.beginDate, this.endDate)
        }
        catch (err) {
            resultValidation.push('incorrect date interval');
        }

        if (resultValidation.length)
            throw new ValidationError(resultValidation.join(', '));
    }

    postExecHandler(sqlData) {

        let strs = [];

        sqlData.forEach(({ dt, hostname, orgname, ...viewsCounters }) => {

            const tmpArr = Object.entries(viewsCounters).map(([key, value]) => {

                const { viewResult, viewType } = this._getViewTypeAndResultBySqlHeader(key);

                return {
                    [this.csvHeaders.date]: moment(dt).format('DD.MM.YYYY'),
                    [this.csvHeaders.viewType]: viewType,
                    [this.csvHeaders.orgName]: orgname,
                    [this.csvHeaders.hostname]: hostname,
                    [this.csvHeaders.viewResult]: viewResult,
                    [this.csvHeaders.count]: value
                }
            });

            strs = strs.concat(tmpArr);
        });

        return strs;
    }

    prepareDataForGraphics(sqlData) {

        const result = {};

        sqlData.forEach(({ dt, ...counters }) => {

            Object.entries(counters).forEach(([key, value]) => {
                if (!result[key])
                    result[key] = [];

                result[key].push({
                    date: moment(dt).format('YYYY-MM-DD'),
                    value: +value
                });
            });
        });

        return result;
    }

    execReport(sqlQuery) {
        return medistModel.executeReportsQuery(sqlQuery);
    }
}

module.exports = async (request) => {

    try {
        const mgtByViews = MgtByViews.create(request);

        mgtByViews.validate();

        await mgtByViews.setHostnames();

        const [sqlDataToCsv, sqlDataToGraphic] = await Promise.all([
            mgtByViews.execReport(mgtByViews.getReportSql()),
            mgtByViews.execReport(mgtByViews.getReportSqlTotalByOrg())
        ]);

        const dataToGraphic = mgtByViews.prepareDataForGraphics(sqlDataToGraphic);
        const dataToFile = mgtByViews.postExecHandler(sqlDataToCsv);

        const path = await helpers.createCSVFile(mgtByViews.getHeaders(), dataToFile, config.workDirMgt, `_отчет_мгт_осмотры_${mgtByViews.getBeginDate()}_${mgtByViews.getEndDate()}`, '');

        return {
            dataToGraphic,
            path: path.split('/').reverse()[0]
        }
    }
    catch (err) {

        console.log(`error througth processing data, reason - ${err.message}, stack - `, err.stack);

        if (err instanceof ValidationError)
            throw createError(400, `error througth validate data, reason - ${err.message}`);
        else
            throw createError(500, 'error througth processing data');
    }
}
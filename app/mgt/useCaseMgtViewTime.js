const createError = require('http-errors');
const moment = require('moment');

const IReport = require('../reportMailing/ReportEntity');

const medistModel = require('../dao/daoPg');
const helpers = require('../utils');
const { ValidationError } = require('../utils/errors');
const utils = require('../utils');
const config = require('../../config');

class MgtByViewTime extends IReport {

    constructor(params) {
        super();

        this.csvHeaders = {
            date: 'Дата',
            viewNumber: '№ Осмотра',
            viewType: 'Тип осмотра',
            totalTime: 'Общее время осмотра',
            viewTime: 'Время осмотра',
            fileTime: 'Время отправки файла',
            singTime: 'Время подписи и печати этикетки',
            orgName: 'Организация',
            hostname: 'Точка осмотра',
            driverFio: 'ФИО сотрудника',
            viewResult: 'Результат осмотра'
        }

        this.sqlHeaders = {
            dt: 'dt',
            avgtime: 'avgtime',
            orgId: 'id'
        }

        this.orgIds = params.orgIds;
        this.beginDate = params.beginDate;
        this.endDate = params.endDate;
    }

    static create(params) {
        return new MgtByViewTime(params);
    }

    getBeginDate() {
        return this.beginDate;
    }

    getEndDate() {
        return this.endDate;
    }

    getReportSql() {

        const query = {
            values: [this.beginDate, this.endDate, this.orgIds],
            text: `SELECT to_date(TO_CHAR(dmr.date, 'DD MM YYYY'), 'DD.MM.YYYY') as "${this.csvHeaders.date}",
            dmr.id as "${this.csvHeaders.viewNumber}",
            (CASE dmd.beforeafter
                WHEN 'before' THEN 'Предрейсовый'
                WHEN 'after' THEN 'Послерейсовый'
                WHEN 'aftershift' THEN 'Послесменный'
                WHEN 'beforeshift' THEN 'Предсменный'
                WHEN 'line' THEN 'Линейный'
             END) as "${this.csvHeaders.viewType}",
          
              TO_CHAR(ut.date - dmd."dateBegin", 'HH24:MI:SS') as "${this.csvHeaders.totalTime}",
          
              TO_CHAR(dmd."dateEnd" - dmd."dateBegin", 'HH24:MI:SS') as "${this.csvHeaders.viewTime}",
          
              TO_CHAR(l.dt - dmd.date, 'HH24:MI:SS') as "${this.csvHeaders.fileTime}",
          
              TO_CHAR(ut.date - l.dt, 'HH24:MI:SS') as "${this.csvHeaders.singTime}",
          
              dmd.hostname as "${this.csvHeaders.hostname}",
          
              dd.surname||' '||dd.name||' '||dd.patronymic as "${this.csvHeaders.driverFio}",
          
              (CASE WHEN dmd.beforeafter IN('before', 'after', 'line') AND dmr."medResult" = true THEN 'допущен'
                WHEN dmd.beforeafter IN('before', 'after', 'line') AND dmr."medResult" = false THEN 'не допущен'
                WHEN dmd.beforeafter IN('beforeshift', 'aftershift') AND dmr."medResult" = false THEN 'не прошел'
                WHEN dmd.beforeafter IN('beforeshift', 'aftershift') AND dmr."medResult" = true THEN 'валидирован'
              END) as "${this.csvHeaders.viewResult}"
               FROM drivers_meddata dmd
                                  JOIN uploadapp_video_data uv ON dmd.id = uv.med_data
                                  join drivers_driver dd ON dmd.driver_id = dd.id
                                  join drivers_organization d_org ON d_org.id = dd.org_id
                join drivers_medresult dmr ON dmr."medData_id" = dmd.id
                                  join (select min(date) dt, "medData_id" from drivers_meddatalog where "ActionState"=4
                                      and date BETWEEN $1::timestamptz AND $2::timestamptz group by "medData_id") l on dmd.id=l."medData_id"
                                  join stamp_history ut ON dmr.id = ut.result_id
                           WHERE d_org.id = ANY($3) AND dmd.beforeafter<>'alcho'
                           ORDER BY dt;`
        }

        return query;
    }

    //TODO add generate series increment pattern (now day), add TO_CHAR view pattern (now YYYY-MM-DD)
    // if date interval more than 1 month add pattern by YYYY-MM and generate series increment 1 month
    // if date interval more than 1 year add pattern YYYY and generate series increment 1 year
    // injected from class conctructor params from request params (maybe) or calculate date interval from request data
    getReportSqlTotalByOrg() {

        const query = {
            values: [this.beginDate, this.endDate, this.orgIds],
            text: `SELECT TO_CHAR(l::date, 'YYYY-MM-DD') as "${this.sqlHeaders.dt}",
                            tt.org_id as "${this.sqlHeaders.orgId}",
                            CASE WHEN tt.avgg IS NULL THEN 0 ELSE tt.avgg END as "${this.sqlHeaders.avgtime}"
                     FROM generate_series($1::timestamptz, $2::timestamptz - '1 day'::interval, '1 day') l 								
                         LEFT JOIN (
                                 SELECT TO_CHAR(dmr.date, 'YYYY-MM-DD') AS dt,
                                        d_org.id as org_id,
                                        ceil( CAST (AVG(EXTRACT(second FROM ut.date - dmd."dateBegin")) as numeric)) AS avgg
                                 FROM drivers_meddata dmd
                                         JOIN drivers_medresult dmr ON dmr."medData_id" = dmd.id
                                         JOIN drivers_driver dd ON dd.id = dmd.driver_id
                                         JOIN drivers_organization d_org ON d_org.id = dd.org_id
                                         left join stamp_history ut ON dmr.id = ut.result_id
                                 WHERE d_org.id = ANY($3) AND dmr.date BETWEEN $1::timestamptz AND $2::timestamptz
                                 GROUP BY dt, d_org.id
                            ) tt ON TO_CHAR(l::date, 'YYYY-MM-DD') = tt.dt;`
        }

        return query;
    }

    getHeaders() {
        return [
            this.csvHeaders.date,
            this.csvHeaders.viewNumber,
            this.csvHeaders.viewType,
            this.csvHeaders.totalTime,
            this.csvHeaders.viewTime,
            this.csvHeaders.fileTime,
            this.csvHeaders.singTime,
            this.csvHeaders.orgName,
            this.csvHeaders.hostname,
            this.csvHeaders.driverFio,
            this.csvHeaders.viewResult
        ];
    }

    validate() {

        const resultValidation = [];

        if (!this.orgIds)
            resultValidation.push('missing orgIds parameter');

        if (resultValidation.length)
            throw new ValidationError(resultValidation.join(', '));

        if (!Array.isArray(this.orgIds))
            resultValidation.push('orgIds type is incorrect. Need array');
        else if (this.orgIds.some(orgId => !Number.isInteger(orgId)))
            resultValidation.push('one or more orgId type is incorrect. Need integer');

        // непродуманный заранее код - говнокод (самобичевание наше все) 
        try {
            utils.validateDate(this.beginDate);
        }
        catch (err) {
            resultValidation.push('beginDate incorrect');
        }

        try {
            utils.validateDate(this.endDate);
        }
        catch (err) {
            resultValidation.push('endDate incorrect');
        }

        try {
            utils.validateIntervalDate(this.beginDate, this.endDate)
        }
        catch (err) {
            resultValidation.push('incorrect date interval');
        }

        if (resultValidation.length)
            throw new ValidationError(resultValidation.join(', '));
    }

    postExecHandler(sqlData) {
        sqlData.forEach(data => {
            data[this.csvHeaders.date] = moment(data[this.csvHeaders.date]).format('DD.MM.YYYY');
        });
    }

    prepareDataForGraphics(sqlData) {

        const result = {};

        sqlData.forEach(({ id, avgtime, dt }) => {
            if (!result[id])
                result[id] = [];

            result[id].push({
                date: dt,
                avgtime: +avgtime
            });
        });

        return result;
    }

    execReport(sqlQuery) {
        return medistModel.executeReportsQuery(sqlQuery);
    }
}

module.exports = async (request) => {

    try {
        const mgtByViewTime = MgtByViewTime.create(request);

        mgtByViewTime.validate();

        const [sqlDataToCsv, sqlDataToGraphic] = await Promise.all([
            mgtByViewTime.execReport(mgtByViewTime.getReportSql()),
            mgtByViewTime.execReport(mgtByViewTime.getReportSqlTotalByOrg())
        ]);

        const dataToGraphic = mgtByViewTime.prepareDataForGraphics(sqlDataToGraphic);
        mgtByViewTime.postExecHandler(sqlDataToCsv);

        const path = await helpers.createCSVFile(mgtByViewTime.getHeaders(), sqlDataToCsv, config.workDirMgt, `_отчет_мгт_время_осмотра_${mgtByViewTime.getBeginDate()}_${mgtByViewTime.getEndDate()}`, '');

        return {
            dataToGraphic,
            path: path.split('/').reverse()[0]
        }
    }
    catch (err) {

        console.log(`error througth processing data, reason - ${err.message}, stack - `, err.stack);

        if (err instanceof ValidationError)
            throw createError(400, `error througth validate data, reason - ${err.message}`);
        else
            throw createError(500, 'error througth processing data');
    }
}